-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 30 2016 г., 20:46
-- Версия сервера: 5.5.50
-- Версия PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `uyua_ankorservis`
--

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_commentmeta`
--

CREATE TABLE IF NOT EXISTS `uyua_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_comments`
--

CREATE TABLE IF NOT EXISTS `uyua_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_links`
--

CREATE TABLE IF NOT EXISTS `uyua_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_options`
--

CREATE TABLE IF NOT EXISTS `uyua_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(191) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `uyua_options`
--

INSERT INTO `uyua_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://slidetest5.webtm.ru', 'yes'),
(2, 'home', 'http://slidetest5.webtm.ru', 'yes'),
(3, 'blogname', 'АнкорСтрой', 'yes'),
(4, 'blogdescription', 'Ещё один сайт на WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'symbolistics@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'd.m.Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'd.m.Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:85:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:0;s:27:"acf-gallery/acf-gallery.php";i:1;s:30:"advanced-custom-fields/acf.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '3', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:80:"C:\\OpenServer\\domains\\slidetest5.webtm.ru/wp-content/plugins/paginate/paginate.php";i:1;s:82:"C:\\OpenServer\\domains\\slidetest5.webtm.ru/wp-content/plugins/acf-gallery/js/input.js";i:2;s:82:"C:\\OpenServer\\domains\\slidetest5.webtm.ru/wp-content/plugins/acf-gallery/gallery.php";i:3;s:86:"C:\\OpenServer\\domains\\slidetest5.webtm.ru/wp-content/plugins/acf-gallery/acf-gallery.php";i:4;s:89:"C:\\OpenServer\\domains\\slidetest5.webtm.ru/wp-content/plugins/advanced-custom-fields/acf.php";}', 'no'),
(40, 'template', 'AnkorService', 'yes'),
(41, 'stylesheet', 'ankorservice', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '37965', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '37965', 'yes'),
(92, 'uyua_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'WPLANG', 'ru_RU', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:18:"orphaned_widgets_1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'cron', 'a:5:{i:1475258746;a:1:{s:26:"upgrader_scheduled_cleanup";a:1:{s:32:"d49a3459eab59251752f2409db1353bc";a:2:{s:8:"schedule";b:0;s:4:"args";a:1:{i:0;i:110;}}}}i:1475295456;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1475338682;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1475340193;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(108, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:65:"https://downloads.wordpress.org/release/ru_RU/wordpress-4.6.1.zip";s:6:"locale";s:5:"ru_RU";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/ru_RU/wordpress-4.6.1.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.6.1";s:7:"version";s:5:"4.6.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.4";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1475252295;s:15:"version_checked";s:5:"4.6.1";s:12:"translations";a:0:{}}', 'no'),
(113, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1475252303;s:7:"checked";a:1:{s:12:"ankorservice";s:5:"0.0.1";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(115, 'can_compress_scripts', '1', 'no'),
(120, '_transient_twentysixteen_categories', '1', 'yes'),
(128, '_transient_timeout_plugin_slugs', '1475338893', 'no'),
(129, '_transient_plugin_slugs', 'a:5:{i:0;s:30:"advanced-custom-fields/acf.php";i:1;s:27:"acf-gallery/acf-gallery.php";i:2;s:19:"akismet/akismet.php";i:3;s:9:"hello.php";i:4;s:36:"wp-pagination/jpages- pagination.php";}', 'no'),
(132, 'theme_mods_twentysixteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1475080062;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(133, 'current_theme', 'АнкорСтрой', 'yes'),
(134, 'theme_mods_ankorservice', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}}', 'yes'),
(135, 'theme_switched', '', 'yes'),
(139, 'category_children', 'a:0:{}', 'yes'),
(156, 'recently_activated', 'a:2:{s:36:"wp-pagination/jpages- pagination.php";i:1475252492;s:21:"paginate/paginate.php";i:1475247059;}', 'yes'),
(159, 'acf_version', '4.4.9', 'yes'),
(162, '_site_transient_timeout_available_translations', '1475174298', 'no');
INSERT INTO `uyua_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(163, '_site_transient_available_translations', 'a:85:{s:2:"ar";a:8:{s:8:"language";s:2:"ar";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-16 18:36:09";s:12:"english_name";s:6:"Arabic";s:11:"native_name";s:14:"العربية";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/ar.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:2;s:3:"ara";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:3:"ary";a:8:{s:8:"language";s:3:"ary";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-21 10:19:10";s:12:"english_name";s:15:"Moroccan Arabic";s:11:"native_name";s:31:"العربية المغربية";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.6.1/ary.zip";s:3:"iso";a:2:{i:1;s:2:"ar";i:3;s:3:"ary";}s:7:"strings";a:1:{s:8:"continue";s:16:"المتابعة";}}s:2:"az";a:8:{s:8:"language";s:2:"az";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-29 08:38:56";s:12:"english_name";s:11:"Azerbaijani";s:11:"native_name";s:16:"Azərbaycan dili";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/az.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:2;s:3:"aze";}s:7:"strings";a:1:{s:8:"continue";s:5:"Davam";}}s:3:"azb";a:8:{s:8:"language";s:3:"azb";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-11 22:42:10";s:12:"english_name";s:17:"South Azerbaijani";s:11:"native_name";s:29:"گؤنئی آذربایجان";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/azb.zip";s:3:"iso";a:2:{i:1;s:2:"az";i:3;s:3:"azb";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"bg_BG";a:8:{s:8:"language";s:5:"bg_BG";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-13 13:34:55";s:12:"english_name";s:9:"Bulgarian";s:11:"native_name";s:18:"Български";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/bg_BG.zip";s:3:"iso";a:2:{i:1;s:2:"bg";i:2;s:3:"bul";}s:7:"strings";a:1:{s:8:"continue";s:12:"Напред";}}s:5:"bn_BD";a:8:{s:8:"language";s:5:"bn_BD";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-15 17:09:07";s:12:"english_name";s:7:"Bengali";s:11:"native_name";s:15:"বাংলা";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/bn_BD.zip";s:3:"iso";a:1:{i:1;s:2:"bn";}s:7:"strings";a:1:{s:8:"continue";s:23:"এগিয়ে চল.";}}s:2:"bo";a:8:{s:8:"language";s:2:"bo";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-05 09:44:12";s:12:"english_name";s:7:"Tibetan";s:11:"native_name";s:21:"བོད་སྐད";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/bo.zip";s:3:"iso";a:2:{i:1;s:2:"bo";i:2;s:3:"tib";}s:7:"strings";a:1:{s:8:"continue";s:24:"མུ་མཐུད།";}}s:5:"bs_BA";a:8:{s:8:"language";s:5:"bs_BA";s:7:"version";s:5:"4.5.4";s:7:"updated";s:19:"2016-04-19 23:16:37";s:12:"english_name";s:7:"Bosnian";s:11:"native_name";s:8:"Bosanski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.4/bs_BA.zip";s:3:"iso";a:2:{i:1;s:2:"bs";i:2;s:3:"bos";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:2:"ca";a:8:{s:8:"language";s:2:"ca";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-07 21:38:31";s:12:"english_name";s:7:"Catalan";s:11:"native_name";s:7:"Català";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/ca.zip";s:3:"iso";a:2:{i:1;s:2:"ca";i:2;s:3:"cat";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:3:"ceb";a:8:{s:8:"language";s:3:"ceb";s:7:"version";s:5:"4.4.3";s:7:"updated";s:19:"2016-02-16 15:34:57";s:12:"english_name";s:7:"Cebuano";s:11:"native_name";s:7:"Cebuano";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.3/ceb.zip";s:3:"iso";a:2:{i:2;s:3:"ceb";i:3;s:3:"ceb";}s:7:"strings";a:1:{s:8:"continue";s:7:"Padayun";}}s:5:"cs_CZ";a:8:{s:8:"language";s:5:"cs_CZ";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-02-11 18:32:36";s:12:"english_name";s:5:"Czech";s:11:"native_name";s:12:"Čeština‎";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.4.2/cs_CZ.zip";s:3:"iso";a:2:{i:1;s:2:"cs";i:2;s:3:"ces";}s:7:"strings";a:1:{s:8:"continue";s:11:"Pokračovat";}}s:2:"cy";a:8:{s:8:"language";s:2:"cy";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-27 09:08:42";s:12:"english_name";s:5:"Welsh";s:11:"native_name";s:7:"Cymraeg";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/cy.zip";s:3:"iso";a:2:{i:1;s:2:"cy";i:2;s:3:"cym";}s:7:"strings";a:1:{s:8:"continue";s:6:"Parhau";}}s:5:"da_DK";a:8:{s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-12 09:12:43";s:12:"english_name";s:6:"Danish";s:11:"native_name";s:5:"Dansk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/da_DK.zip";s:3:"iso";a:2:{i:1;s:2:"da";i:2;s:3:"dan";}s:7:"strings";a:1:{s:8:"continue";s:12:"Forts&#230;t";}}s:14:"de_CH_informal";a:8:{s:8:"language";s:14:"de_CH_informal";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-15 12:59:43";s:12:"english_name";s:23:"(Switzerland, Informal)";s:11:"native_name";s:21:"Deutsch (Schweiz, Du)";s:7:"package";s:73:"https://downloads.wordpress.org/translation/core/4.6.1/de_CH_informal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:5:"de_DE";a:8:{s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-17 20:12:38";s:12:"english_name";s:6:"German";s:11:"native_name";s:7:"Deutsch";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/de_DE.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:12:"de_DE_formal";a:8:{s:8:"language";s:12:"de_DE_formal";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-22 18:59:12";s:12:"english_name";s:15:"German (Formal)";s:11:"native_name";s:13:"Deutsch (Sie)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.6.1/de_DE_formal.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:5:"de_CH";a:8:{s:8:"language";s:5:"de_CH";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-15 12:56:13";s:12:"english_name";s:20:"German (Switzerland)";s:11:"native_name";s:17:"Deutsch (Schweiz)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/de_CH.zip";s:3:"iso";a:1:{i:1;s:2:"de";}s:7:"strings";a:1:{s:8:"continue";s:6:"Weiter";}}s:2:"el";a:8:{s:8:"language";s:2:"el";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-29 09:46:56";s:12:"english_name";s:5:"Greek";s:11:"native_name";s:16:"Ελληνικά";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/el.zip";s:3:"iso";a:2:{i:1;s:2:"el";i:2;s:3:"ell";}s:7:"strings";a:1:{s:8:"continue";s:16:"Συνέχεια";}}s:5:"en_ZA";a:8:{s:8:"language";s:5:"en_ZA";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-16 11:54:12";s:12:"english_name";s:22:"English (South Africa)";s:11:"native_name";s:22:"English (South Africa)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/en_ZA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_NZ";a:8:{s:8:"language";s:5:"en_NZ";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-20 07:14:07";s:12:"english_name";s:21:"English (New Zealand)";s:11:"native_name";s:21:"English (New Zealand)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/en_NZ.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_GB";a:8:{s:8:"language";s:5:"en_GB";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-11 22:36:25";s:12:"english_name";s:12:"English (UK)";s:11:"native_name";s:12:"English (UK)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/en_GB.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_CA";a:8:{s:8:"language";s:5:"en_CA";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-11 23:19:29";s:12:"english_name";s:16:"English (Canada)";s:11:"native_name";s:16:"English (Canada)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/en_CA.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:5:"en_AU";a:8:{s:8:"language";s:5:"en_AU";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-12 02:18:44";s:12:"english_name";s:19:"English (Australia)";s:11:"native_name";s:19:"English (Australia)";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/en_AU.zip";s:3:"iso";a:3:{i:1;s:2:"en";i:2;s:3:"eng";i:3;s:3:"eng";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continue";}}s:2:"eo";a:8:{s:8:"language";s:2:"eo";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-23 13:24:01";s:12:"english_name";s:9:"Esperanto";s:11:"native_name";s:9:"Esperanto";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/eo.zip";s:3:"iso";a:2:{i:1;s:2:"eo";i:2;s:3:"epo";}s:7:"strings";a:1:{s:8:"continue";s:8:"Daŭrigi";}}s:5:"es_AR";a:8:{s:8:"language";s:5:"es_AR";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-19 13:48:04";s:12:"english_name";s:19:"Spanish (Argentina)";s:11:"native_name";s:21:"Español de Argentina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/es_AR.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_ES";a:8:{s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-28 18:21:58";s:12:"english_name";s:15:"Spanish (Spain)";s:11:"native_name";s:8:"Español";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/es_ES.zip";s:3:"iso";a:1:{i:1;s:2:"es";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_PE";a:8:{s:8:"language";s:5:"es_PE";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-09 09:36:22";s:12:"english_name";s:14:"Spanish (Peru)";s:11:"native_name";s:17:"Español de Perú";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/es_PE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CL";a:8:{s:8:"language";s:5:"es_CL";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-17 22:11:44";s:12:"english_name";s:15:"Spanish (Chile)";s:11:"native_name";s:17:"Español de Chile";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/es_CL.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_CO";a:8:{s:8:"language";s:5:"es_CO";s:7:"version";s:6:"4.3-RC";s:7:"updated";s:19:"2015-08-04 06:10:33";s:12:"english_name";s:18:"Spanish (Colombia)";s:11:"native_name";s:20:"Español de Colombia";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.3-RC/es_CO.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_GT";a:8:{s:8:"language";s:5:"es_GT";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-17 17:56:31";s:12:"english_name";s:19:"Spanish (Guatemala)";s:11:"native_name";s:21:"Español de Guatemala";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/es_GT.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_VE";a:8:{s:8:"language";s:5:"es_VE";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-17 12:34:44";s:12:"english_name";s:19:"Spanish (Venezuela)";s:11:"native_name";s:21:"Español de Venezuela";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/es_VE.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"es_MX";a:8:{s:8:"language";s:5:"es_MX";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-29 15:07:52";s:12:"english_name";s:16:"Spanish (Mexico)";s:11:"native_name";s:19:"Español de México";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/es_MX.zip";s:3:"iso";a:2:{i:1;s:2:"es";i:2;s:3:"spa";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"et";a:8:{s:8:"language";s:2:"et";s:7:"version";s:7:"4.6-rc2";s:7:"updated";s:19:"2016-08-09 06:49:25";s:12:"english_name";s:8:"Estonian";s:11:"native_name";s:5:"Eesti";s:7:"package";s:63:"https://downloads.wordpress.org/translation/core/4.6-rc2/et.zip";s:3:"iso";a:2:{i:1;s:2:"et";i:2;s:3:"est";}s:7:"strings";a:1:{s:8:"continue";s:6:"Jätka";}}s:2:"eu";a:8:{s:8:"language";s:2:"eu";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-08 19:27:09";s:12:"english_name";s:6:"Basque";s:11:"native_name";s:7:"Euskara";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/eu.zip";s:3:"iso";a:2:{i:1;s:2:"eu";i:2;s:3:"eus";}s:7:"strings";a:1:{s:8:"continue";s:8:"Jarraitu";}}s:5:"fa_IR";a:8:{s:8:"language";s:5:"fa_IR";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-16 09:07:28";s:12:"english_name";s:7:"Persian";s:11:"native_name";s:10:"فارسی";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/fa_IR.zip";s:3:"iso";a:2:{i:1;s:2:"fa";i:2;s:3:"fas";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:2:"fi";a:8:{s:8:"language";s:2:"fi";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-15 18:30:48";s:12:"english_name";s:7:"Finnish";s:11:"native_name";s:5:"Suomi";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/fi.zip";s:3:"iso";a:2:{i:1;s:2:"fi";i:2;s:3:"fin";}s:7:"strings";a:1:{s:8:"continue";s:5:"Jatka";}}s:5:"fr_CA";a:8:{s:8:"language";s:5:"fr_CA";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-15 19:02:20";s:12:"english_name";s:15:"French (Canada)";s:11:"native_name";s:19:"Français du Canada";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/fr_CA.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_FR";a:8:{s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-22 09:07:58";s:12:"english_name";s:15:"French (France)";s:11:"native_name";s:9:"Français";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/fr_FR.zip";s:3:"iso";a:1:{i:1;s:2:"fr";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:5:"fr_BE";a:8:{s:8:"language";s:5:"fr_BE";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-13 16:21:14";s:12:"english_name";s:16:"French (Belgium)";s:11:"native_name";s:21:"Français de Belgique";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/fr_BE.zip";s:3:"iso";a:2:{i:1;s:2:"fr";i:2;s:3:"fra";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuer";}}s:2:"gd";a:8:{s:8:"language";s:2:"gd";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-23 17:41:37";s:12:"english_name";s:15:"Scottish Gaelic";s:11:"native_name";s:9:"Gàidhlig";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/gd.zip";s:3:"iso";a:3:{i:1;s:2:"gd";i:2;s:3:"gla";i:3;s:3:"gla";}s:7:"strings";a:1:{s:8:"continue";s:15:"Lean air adhart";}}s:5:"gl_ES";a:8:{s:8:"language";s:5:"gl_ES";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-21 15:44:17";s:12:"english_name";s:8:"Galician";s:11:"native_name";s:6:"Galego";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/gl_ES.zip";s:3:"iso";a:2:{i:1;s:2:"gl";i:2;s:3:"glg";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:2:"gu";a:8:{s:8:"language";s:2:"gu";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-22 06:08:05";s:12:"english_name";s:8:"Gujarati";s:11:"native_name";s:21:"ગુજરાતી";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/gu.zip";s:3:"iso";a:2:{i:1;s:2:"gu";i:2;s:3:"guj";}s:7:"strings";a:1:{s:8:"continue";s:31:"ચાલુ રાખવું";}}s:3:"haz";a:8:{s:8:"language";s:3:"haz";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-05 00:59:09";s:12:"english_name";s:8:"Hazaragi";s:11:"native_name";s:15:"هزاره گی";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip";s:3:"iso";a:1:{i:3;s:3:"haz";}s:7:"strings";a:1:{s:8:"continue";s:10:"ادامه";}}s:5:"he_IL";a:8:{s:8:"language";s:5:"he_IL";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-25 19:56:49";s:12:"english_name";s:6:"Hebrew";s:11:"native_name";s:16:"עִבְרִית";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/he_IL.zip";s:3:"iso";a:1:{i:1;s:2:"he";}s:7:"strings";a:1:{s:8:"continue";s:8:"המשך";}}s:5:"hi_IN";a:8:{s:8:"language";s:5:"hi_IN";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-03 13:43:01";s:12:"english_name";s:5:"Hindi";s:11:"native_name";s:18:"हिन्दी";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/hi_IN.zip";s:3:"iso";a:2:{i:1;s:2:"hi";i:2;s:3:"hin";}s:7:"strings";a:1:{s:8:"continue";s:12:"जारी";}}s:2:"hr";a:8:{s:8:"language";s:2:"hr";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-07 15:12:28";s:12:"english_name";s:8:"Croatian";s:11:"native_name";s:8:"Hrvatski";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/hr.zip";s:3:"iso";a:2:{i:1;s:2:"hr";i:2;s:3:"hrv";}s:7:"strings";a:1:{s:8:"continue";s:7:"Nastavi";}}s:5:"hu_HU";a:8:{s:8:"language";s:5:"hu_HU";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-07 19:31:42";s:12:"english_name";s:9:"Hungarian";s:11:"native_name";s:6:"Magyar";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/hu_HU.zip";s:3:"iso";a:2:{i:1;s:2:"hu";i:2;s:3:"hun";}s:7:"strings";a:1:{s:8:"continue";s:10:"Folytatás";}}s:2:"hy";a:8:{s:8:"language";s:2:"hy";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2016-02-04 07:13:54";s:12:"english_name";s:8:"Armenian";s:11:"native_name";s:14:"Հայերեն";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.4.2/hy.zip";s:3:"iso";a:2:{i:1;s:2:"hy";i:2;s:3:"hye";}s:7:"strings";a:1:{s:8:"continue";s:20:"Շարունակել";}}s:5:"id_ID";a:8:{s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-22 05:34:53";s:12:"english_name";s:10:"Indonesian";s:11:"native_name";s:16:"Bahasa Indonesia";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/id_ID.zip";s:3:"iso";a:2:{i:1;s:2:"id";i:2;s:3:"ind";}s:7:"strings";a:1:{s:8:"continue";s:9:"Lanjutkan";}}s:5:"is_IS";a:8:{s:8:"language";s:5:"is_IS";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-24 19:04:04";s:12:"english_name";s:9:"Icelandic";s:11:"native_name";s:9:"Íslenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/is_IS.zip";s:3:"iso";a:2:{i:1;s:2:"is";i:2;s:3:"isl";}s:7:"strings";a:1:{s:8:"continue";s:6:"Áfram";}}s:5:"it_IT";a:8:{s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-09 15:46:50";s:12:"english_name";s:7:"Italian";s:11:"native_name";s:8:"Italiano";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/it_IT.zip";s:3:"iso";a:2:{i:1;s:2:"it";i:2;s:3:"ita";}s:7:"strings";a:1:{s:8:"continue";s:8:"Continua";}}s:2:"ja";a:8:{s:8:"language";s:2:"ja";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-17 17:33:07";s:12:"english_name";s:8:"Japanese";s:11:"native_name";s:9:"日本語";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/ja.zip";s:3:"iso";a:1:{i:1;s:2:"ja";}s:7:"strings";a:1:{s:8:"continue";s:9:"続ける";}}s:5:"ka_GE";a:8:{s:8:"language";s:5:"ka_GE";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-29 11:51:34";s:12:"english_name";s:8:"Georgian";s:11:"native_name";s:21:"ქართული";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/ka_GE.zip";s:3:"iso";a:2:{i:1;s:2:"ka";i:2;s:3:"kat";}s:7:"strings";a:1:{s:8:"continue";s:30:"გაგრძელება";}}s:5:"ko_KR";a:8:{s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-24 07:18:31";s:12:"english_name";s:6:"Korean";s:11:"native_name";s:9:"한국어";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/ko_KR.zip";s:3:"iso";a:2:{i:1;s:2:"ko";i:2;s:3:"kor";}s:7:"strings";a:1:{s:8:"continue";s:6:"계속";}}s:5:"lt_LT";a:8:{s:8:"language";s:5:"lt_LT";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-11 21:29:34";s:12:"english_name";s:10:"Lithuanian";s:11:"native_name";s:15:"Lietuvių kalba";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/lt_LT.zip";s:3:"iso";a:2:{i:1;s:2:"lt";i:2;s:3:"lit";}s:7:"strings";a:1:{s:8:"continue";s:6:"Tęsti";}}s:2:"lv";a:8:{s:8:"language";s:2:"lv";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-20 15:33:17";s:12:"english_name";s:7:"Latvian";s:11:"native_name";s:16:"Latviešu valoda";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/lv.zip";s:3:"iso";a:2:{i:1;s:2:"lv";i:2;s:3:"lav";}s:7:"strings";a:1:{s:8:"continue";s:9:"Turpināt";}}s:5:"mk_MK";a:8:{s:8:"language";s:5:"mk_MK";s:7:"version";s:5:"4.5.4";s:7:"updated";s:19:"2016-05-12 13:55:28";s:12:"english_name";s:10:"Macedonian";s:11:"native_name";s:31:"Македонски јазик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.4/mk_MK.zip";s:3:"iso";a:2:{i:1;s:2:"mk";i:2;s:3:"mkd";}s:7:"strings";a:1:{s:8:"continue";s:16:"Продолжи";}}s:2:"mr";a:8:{s:8:"language";s:2:"mr";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-14 07:43:21";s:12:"english_name";s:7:"Marathi";s:11:"native_name";s:15:"मराठी";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/mr.zip";s:3:"iso";a:2:{i:1;s:2:"mr";i:2;s:3:"mar";}s:7:"strings";a:1:{s:8:"continue";s:25:"सुरु ठेवा";}}s:5:"ms_MY";a:8:{s:8:"language";s:5:"ms_MY";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-14 14:18:43";s:12:"english_name";s:5:"Malay";s:11:"native_name";s:13:"Bahasa Melayu";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/ms_MY.zip";s:3:"iso";a:2:{i:1;s:2:"ms";i:2;s:3:"msa";}s:7:"strings";a:1:{s:8:"continue";s:8:"Teruskan";}}s:5:"my_MM";a:8:{s:8:"language";s:5:"my_MM";s:7:"version";s:6:"4.1.13";s:7:"updated";s:19:"2015-03-26 15:57:42";s:12:"english_name";s:17:"Myanmar (Burmese)";s:11:"native_name";s:15:"ဗမာစာ";s:7:"package";s:65:"https://downloads.wordpress.org/translation/core/4.1.13/my_MM.zip";s:3:"iso";a:2:{i:1;s:2:"my";i:2;s:3:"mya";}s:7:"strings";a:1:{s:8:"continue";s:54:"ဆက်လက်လုပ်ဆောင်ပါ။";}}s:5:"nb_NO";a:8:{s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-16 13:09:49";s:12:"english_name";s:19:"Norwegian (Bokmål)";s:11:"native_name";s:13:"Norsk bokmål";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/nb_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nb";i:2;s:3:"nob";}s:7:"strings";a:1:{s:8:"continue";s:8:"Fortsett";}}s:5:"nl_NL";a:8:{s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-28 10:53:14";s:12:"english_name";s:5:"Dutch";s:11:"native_name";s:10:"Nederlands";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/nl_NL.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:12:"nl_NL_formal";a:8:{s:8:"language";s:12:"nl_NL_formal";s:7:"version";s:5:"4.4.3";s:7:"updated";s:19:"2016-01-20 13:35:50";s:12:"english_name";s:14:"Dutch (Formal)";s:11:"native_name";s:20:"Nederlands (Formeel)";s:7:"package";s:71:"https://downloads.wordpress.org/translation/core/4.4.3/nl_NL_formal.zip";s:3:"iso";a:2:{i:1;s:2:"nl";i:2;s:3:"nld";}s:7:"strings";a:1:{s:8:"continue";s:8:"Doorgaan";}}s:5:"nn_NO";a:8:{s:8:"language";s:5:"nn_NO";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-27 16:24:28";s:12:"english_name";s:19:"Norwegian (Nynorsk)";s:11:"native_name";s:13:"Norsk nynorsk";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/nn_NO.zip";s:3:"iso";a:2:{i:1;s:2:"nn";i:2;s:3:"nno";}s:7:"strings";a:1:{s:8:"continue";s:9:"Hald fram";}}s:3:"oci";a:8:{s:8:"language";s:3:"oci";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-23 13:45:11";s:12:"english_name";s:7:"Occitan";s:11:"native_name";s:7:"Occitan";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.6.1/oci.zip";s:3:"iso";a:2:{i:1;s:2:"oc";i:2;s:3:"oci";}s:7:"strings";a:1:{s:8:"continue";s:9:"Contunhar";}}s:5:"pl_PL";a:8:{s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-22 09:54:16";s:12:"english_name";s:6:"Polish";s:11:"native_name";s:6:"Polski";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/pl_PL.zip";s:3:"iso";a:2:{i:1;s:2:"pl";i:2;s:3:"pol";}s:7:"strings";a:1:{s:8:"continue";s:9:"Kontynuuj";}}s:2:"ps";a:8:{s:8:"language";s:2:"ps";s:7:"version";s:6:"4.1.13";s:7:"updated";s:19:"2015-03-29 22:19:48";s:12:"english_name";s:6:"Pashto";s:11:"native_name";s:8:"پښتو";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.1.13/ps.zip";s:3:"iso";a:2:{i:1;s:2:"ps";i:2;s:3:"pus";}s:7:"strings";a:1:{s:8:"continue";s:19:"دوام ورکړه";}}s:5:"pt_BR";a:8:{s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-27 23:21:41";s:12:"english_name";s:19:"Portuguese (Brazil)";s:11:"native_name";s:20:"Português do Brasil";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/pt_BR.zip";s:3:"iso";a:2:{i:1;s:2:"pt";i:2;s:3:"por";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"pt_PT";a:8:{s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-14 09:00:10";s:12:"english_name";s:21:"Portuguese (Portugal)";s:11:"native_name";s:10:"Português";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/pt_PT.zip";s:3:"iso";a:1:{i:1;s:2:"pt";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuar";}}s:5:"ro_RO";a:8:{s:8:"language";s:5:"ro_RO";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-14 19:25:04";s:12:"english_name";s:8:"Romanian";s:11:"native_name";s:8:"Română";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/ro_RO.zip";s:3:"iso";a:2:{i:1;s:2:"ro";i:2;s:3:"ron";}s:7:"strings";a:1:{s:8:"continue";s:9:"Continuă";}}s:5:"ru_RU";a:8:{s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-30 19:40:04";s:12:"english_name";s:7:"Russian";s:11:"native_name";s:14:"Русский";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/ru_RU.zip";s:3:"iso";a:2:{i:1;s:2:"ru";i:2;s:3:"rus";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продолжить";}}s:5:"sk_SK";a:8:{s:8:"language";s:5:"sk_SK";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-17 10:49:08";s:12:"english_name";s:6:"Slovak";s:11:"native_name";s:11:"Slovenčina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/sk_SK.zip";s:3:"iso";a:2:{i:1;s:2:"sk";i:2;s:3:"slk";}s:7:"strings";a:1:{s:8:"continue";s:12:"Pokračovať";}}s:5:"sl_SI";a:8:{s:8:"language";s:5:"sl_SI";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-18 16:23:26";s:12:"english_name";s:9:"Slovenian";s:11:"native_name";s:13:"Slovenščina";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/sl_SI.zip";s:3:"iso";a:2:{i:1;s:2:"sl";i:2;s:3:"slv";}s:7:"strings";a:1:{s:8:"continue";s:8:"Nadaljuj";}}s:2:"sq";a:8:{s:8:"language";s:2:"sq";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-14 07:00:01";s:12:"english_name";s:8:"Albanian";s:11:"native_name";s:5:"Shqip";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/sq.zip";s:3:"iso";a:2:{i:1;s:2:"sq";i:2;s:3:"sqi";}s:7:"strings";a:1:{s:8:"continue";s:6:"Vazhdo";}}s:5:"sr_RS";a:8:{s:8:"language";s:5:"sr_RS";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-12 16:41:17";s:12:"english_name";s:7:"Serbian";s:11:"native_name";s:23:"Српски језик";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/sr_RS.zip";s:3:"iso";a:2:{i:1;s:2:"sr";i:2;s:3:"srp";}s:7:"strings";a:1:{s:8:"continue";s:14:"Настави";}}s:5:"sv_SE";a:8:{s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-29 20:20:44";s:12:"english_name";s:7:"Swedish";s:11:"native_name";s:7:"Svenska";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/sv_SE.zip";s:3:"iso";a:2:{i:1;s:2:"sv";i:2;s:3:"swe";}s:7:"strings";a:1:{s:8:"continue";s:9:"Fortsätt";}}s:3:"szl";a:8:{s:8:"language";s:3:"szl";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-24 19:58:14";s:12:"english_name";s:8:"Silesian";s:11:"native_name";s:17:"Ślōnskŏ gŏdka";s:7:"package";s:62:"https://downloads.wordpress.org/translation/core/4.6.1/szl.zip";s:3:"iso";a:1:{i:3;s:3:"szl";}s:7:"strings";a:1:{s:8:"continue";s:13:"Kōntynuować";}}s:2:"th";a:8:{s:8:"language";s:2:"th";s:7:"version";s:5:"4.5.4";s:7:"updated";s:19:"2016-06-30 10:22:26";s:12:"english_name";s:4:"Thai";s:11:"native_name";s:9:"ไทย";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.5.4/th.zip";s:3:"iso";a:2:{i:1;s:2:"th";i:2;s:3:"tha";}s:7:"strings";a:1:{s:8:"continue";s:15:"ต่อไป";}}s:2:"tl";a:8:{s:8:"language";s:2:"tl";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-11-27 15:51:36";s:12:"english_name";s:7:"Tagalog";s:11:"native_name";s:7:"Tagalog";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.4.2/tl.zip";s:3:"iso";a:2:{i:1;s:2:"tl";i:2;s:3:"tgl";}s:7:"strings";a:1:{s:8:"continue";s:10:"Magpatuloy";}}s:5:"tr_TR";a:8:{s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-16 10:50:15";s:12:"english_name";s:7:"Turkish";s:11:"native_name";s:8:"Türkçe";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/tr_TR.zip";s:3:"iso";a:2:{i:1;s:2:"tr";i:2;s:3:"tur";}s:7:"strings";a:1:{s:8:"continue";s:5:"Devam";}}s:5:"ug_CN";a:8:{s:8:"language";s:5:"ug_CN";s:7:"version";s:5:"4.5.4";s:7:"updated";s:19:"2016-06-22 12:27:05";s:12:"english_name";s:6:"Uighur";s:11:"native_name";s:9:"Uyƣurqə";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.4/ug_CN.zip";s:3:"iso";a:2:{i:1;s:2:"ug";i:2;s:3:"uig";}s:7:"strings";a:1:{s:8:"continue";s:26:"داۋاملاشتۇرۇش";}}s:2:"uk";a:8:{s:8:"language";s:2:"uk";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-26 14:31:53";s:12:"english_name";s:9:"Ukrainian";s:11:"native_name";s:20:"Українська";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/uk.zip";s:3:"iso";a:2:{i:1;s:2:"uk";i:2;s:3:"ukr";}s:7:"strings";a:1:{s:8:"continue";s:20:"Продовжити";}}s:2:"vi";a:8:{s:8:"language";s:2:"vi";s:7:"version";s:5:"4.4.2";s:7:"updated";s:19:"2015-12-09 01:01:25";s:12:"english_name";s:10:"Vietnamese";s:11:"native_name";s:14:"Tiếng Việt";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.4.2/vi.zip";s:3:"iso";a:2:{i:1;s:2:"vi";i:2;s:3:"vie";}s:7:"strings";a:1:{s:8:"continue";s:12:"Tiếp tục";}}s:5:"zh_TW";a:8:{s:8:"language";s:5:"zh_TW";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-08-18 13:53:15";s:12:"english_name";s:16:"Chinese (Taiwan)";s:11:"native_name";s:12:"繁體中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/zh_TW.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"繼續";}}s:5:"zh_CN";a:8:{s:8:"language";s:5:"zh_CN";s:7:"version";s:5:"4.5.4";s:7:"updated";s:19:"2016-04-17 03:29:01";s:12:"english_name";s:15:"Chinese (China)";s:11:"native_name";s:12:"简体中文";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.5.4/zh_CN.zip";s:3:"iso";a:2:{i:1;s:2:"zh";i:2;s:3:"zho";}s:7:"strings";a:1:{s:8:"continue";s:6:"继续";}}}', 'no'),
(166, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(175, '_transient_timeout_feed_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1475286633', 'no'),
(176, '_transient_timeout_feed_mod_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1475286633', 'no'),
(177, '_transient_feed_mod_ac0b00fe65abe10e0c5b588f3ed8c7ca', '1475243433', 'no'),
(178, '_transient_timeout_feed_d117b5738fbd35bd8c0391cda1f2b5d9', '1475286635', 'no'),
(179, '_transient_timeout_feed_mod_d117b5738fbd35bd8c0391cda1f2b5d9', '1475286635', 'no'),
(180, '_transient_feed_mod_d117b5738fbd35bd8c0391cda1f2b5d9', '1475243435', 'no'),
(181, '_transient_timeout_feed_b9388c83948825c1edaef0d856b7b109', '1475286637', 'no');
INSERT INTO `uyua_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(182, '_transient_feed_b9388c83948825c1edaef0d856b7b109', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"\n	\n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:117:"\n		\n		\n		\n		\n		\n		\n				\n\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n		\n\n	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"WordPress Plugins » View: Popular";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:45:"https://wordpress.org/plugins/browse/popular/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:34:"WordPress Plugins » View: Popular";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 30 Sep 2016 13:35:14 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:25:"http://bbpress.org/?v=1.1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:30:{i:0;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:9:"Yoast SEO";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://wordpress.org/plugins/wordpress-seo/#post-8321";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 01 Jan 2009 20:34:44 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"8321@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:114:"Improve your WordPress SEO: Write better content and have a fully optimized WordPress site using Yoast SEO plugin.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Joost de Valk";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"WordPress Importer";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://wordpress.org/plugins/wordpress-importer/#post-18101";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 20 May 2010 17:42:45 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"18101@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:101:"Import posts, pages, comments, custom fields, categories, tags and more from a WordPress export file.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Brian Colinger";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:26:"Page Builder by SiteOrigin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:59:"https://wordpress.org/plugins/siteorigin-panels/#post-51888";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 11 Apr 2013 10:36:42 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"51888@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:111:"Build responsive page layouts using the widgets you know and love using this simple drag and drop page builder.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Greg Priday";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"Hello Dolly";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:52:"https://wordpress.org/plugins/hello-dolly/#post-5790";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 29 May 2008 22:11:34 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"5790@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:150:"This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Matt Mullenweg";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"Contact Form 7";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://wordpress.org/plugins/contact-form-7/#post-2141";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 02 Aug 2007 12:45:03 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2141@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:54:"Just another contact form plugin. Simple but flexible.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Takayuki Miyoshi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:21:"Regenerate Thumbnails";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"https://wordpress.org/plugins/regenerate-thumbnails/#post-6743";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 23 Aug 2008 14:38:58 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"6743@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:76:"Allows you to regenerate your thumbnails after changing the thumbnail sizes.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:25:"Alex Mills (Viper007Bond)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"W3 Total Cache";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wordpress.org/plugins/w3-total-cache/#post-12073";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 29 Jul 2009 18:46:31 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"12073@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:132:"Easy Web Performance Optimization (WPO) using caching: browser, page, object, database, minify and content delivery network support.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Frederick Townes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:15:"NextGEN Gallery";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:56:"https://wordpress.org/plugins/nextgen-gallery/#post-1169";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 23 Apr 2007 20:08:06 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"1169@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:121:"The most popular WordPress gallery plugin and one of the most popular plugins of all time with over 15 million downloads.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Alex Rabe";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"Google Analytics by MonsterInsights";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://wordpress.org/plugins/google-analytics-for-wordpress/#post-2316";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 14 Sep 2007 12:15:27 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2316@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:113:"Connect Google Analytics with WordPress by adding your Google Analytics tracking code. Get the stats that matter.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Syed Balkhi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"WooCommerce";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wordpress.org/plugins/woocommerce/#post-29860";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 05 Sep 2011 08:13:36 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"29860@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:97:"WooCommerce is a powerful, extendable eCommerce plugin that helps you sell anything. Beautifully.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"WooThemes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:24:"Jetpack by WordPress.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:49:"https://wordpress.org/plugins/jetpack/#post-23862";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 20 Jan 2011 02:21:38 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"23862@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:107:"Increase your traffic, view your stats, speed up your site, and protect yourself from hackers with Jetpack.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Automattic";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:7:"Akismet";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:46:"https://wordpress.org/plugins/akismet/#post-15";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 09 Mar 2007 22:11:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:33:"15@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:98:"Akismet checks your comments against the Akismet Web service to see if they look like spam or not.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Matt Mullenweg";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"WP Super Cache";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://wordpress.org/plugins/wp-super-cache/#post-2572";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 05 Nov 2007 11:40:04 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2572@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:73:"A very fast caching engine for WordPress that produces static html files.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Donncha O Caoimh";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"WP-PageNavi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wordpress.org/plugins/wp-pagenavi/#post-363";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 09 Mar 2007 23:17:57 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"363@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:49:"Adds a more advanced paging navigation interface.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Lester Chan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:21:"Really Simple CAPTCHA";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"https://wordpress.org/plugins/really-simple-captcha/#post-9542";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 09 Mar 2009 02:17:35 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"9542@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:138:"Really Simple CAPTCHA is a CAPTCHA module intended to be called from other plugins. It is originally created for my Contact Form 7 plugin.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:16:"Takayuki Miyoshi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:15;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"Wordfence Security";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wordpress.org/plugins/wordfence/#post-29832";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 04 Sep 2011 03:13:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"29832@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:150:"Secure your website with the most comprehensive WordPress security plugin. Firewall, malware scanner, blocking, live traffic, login security &#38; mor";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Wordfence";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:16;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:14:"Duplicate Post";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:55:"https://wordpress.org/plugins/duplicate-post/#post-2646";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 05 Dec 2007 17:40:03 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2646@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:22:"Clone posts and pages.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:4:"Lopo";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:17;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"TinyMCE Advanced";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:57:"https://wordpress.org/plugins/tinymce-advanced/#post-2082";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 27 Jun 2007 15:00:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"2082@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:58:"Extends and enhances TinyMCE, the WordPress Visual Editor.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Andrew Ozz";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:18;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"All in One SEO Pack";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:59:"https://wordpress.org/plugins/all-in-one-seo-pack/#post-753";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 30 Mar 2007 20:08:18 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"753@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:150:"The most downloaded plugin for WordPress (almost 30 million downloads). Use All in One SEO Pack to automatically optimize your site for Search Engines";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:8:"uberdose";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:19;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"Google XML Sitemaps";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://wordpress.org/plugins/google-sitemap-generator/#post-132";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 09 Mar 2007 22:31:32 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:34:"132@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:105:"This plugin will generate a special XML sitemap which will help search engines to better index your blog.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Arne Brachhold";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:20;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:22:"Advanced Custom Fields";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:64:"https://wordpress.org/plugins/advanced-custom-fields/#post-25254";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 17 Mar 2011 04:07:30 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"25254@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:68:"Customise WordPress with powerful, professional and intuitive fields";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"elliotcondon";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:21;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:30:"Clef Two-Factor Authentication";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:48:"https://wordpress.org/plugins/wpclef/#post-47509";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 27 Dec 2012 01:25:57 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"47509@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:139:"Modern two-factor that people love to use: strong authentication without passwords or tokens; single sign on/off; magical login experience.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"Dave Ross";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:22;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:35:"UpdraftPlus WordPress Backup Plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:53:"https://wordpress.org/plugins/updraftplus/#post-38058";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 21 May 2012 15:14:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"38058@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:148:"Backup and restoration made easy. Complete backups; manual or scheduled (backup to S3, Dropbox, Google Drive, Rackspace, FTP, SFTP, email + others).";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"David Anderson";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:23;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"Disable Comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:58:"https://wordpress.org/plugins/disable-comments/#post-26907";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 27 May 2011 04:42:58 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"26907@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:134:"Allows administrators to globally disable comments on their site. Comments can be disabled according to post type. Multisite friendly.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Samir Shah";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:24;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:33:"Google Analytics Dashboard for WP";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:75:"https://wordpress.org/plugins/google-analytics-dashboard-for-wp/#post-50539";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 10 Mar 2013 17:07:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"50539@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:127:"Displays Google Analytics reports in your WordPress Dashboard. Inserts the latest Google Analytics tracking code in your pages.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Alin Marcu";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:25;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"WP Multibyte Patch";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://wordpress.org/plugins/wp-multibyte-patch/#post-28395";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 14 Jul 2011 12:22:53 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"28395@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:71:"Multibyte functionality enhancement for the WordPress Japanese package.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"plugin-master";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:26;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:27:"Black Studio TinyMCE Widget";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:69:"https://wordpress.org/plugins/black-studio-tinymce-widget/#post-31973";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 10 Nov 2011 15:06:14 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"31973@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:39:"The visual editor widget for Wordpress.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Marco Chiesi";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:27;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:10:"Duplicator";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:52:"https://wordpress.org/plugins/duplicator/#post-26607";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 16 May 2011 12:15:41 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"26607@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:88:"Duplicate, clone, backup, move and transfer an entire site from one location to another.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:10:"Cory Lamle";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:28;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:46:"iThemes Security (formerly Better WP Security)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:60:"https://wordpress.org/plugins/better-wp-security/#post-21738";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 22 Oct 2010 22:06:05 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"21738@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:146:"Take the guesswork out of WordPress security. iThemes Security offers 30+ ways to lock down WordPress in an easy-to-use WordPress security plugin.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"iThemes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:29;a:6:{s:4:"data";s:30:"\n			\n			\n			\n			\n			\n			\n					";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:2:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"Meta Slider";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:51:"https://wordpress.org/plugins/ml-slider/#post-49521";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 14 Feb 2013 16:56:31 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"49521@https://wordpress.org/plugins/";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:131:"Easy to use WordPress Slider plugin. Create responsive slideshows with Nivo Slider, Flex Slider, Coin Slider and Responsive Slides.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Matcha Labs";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:46:"https://wordpress.org/plugins/rss/view/popular";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";O:42:"Requests_Utility_CaseInsensitiveDictionary":1:{s:7:"\0*\0data";a:12:{s:6:"server";s:5:"nginx";s:4:"date";s:29:"Fri, 30 Sep 2016 13:50:36 GMT";s:12:"content-type";s:23:"text/xml; charset=UTF-8";s:4:"vary";s:15:"Accept-Encoding";s:25:"strict-transport-security";s:11:"max-age=360";s:7:"expires";s:29:"Fri, 30 Sep 2016 14:10:14 GMT";s:13:"cache-control";s:0:"";s:6:"pragma";s:0:"";s:13:"last-modified";s:31:"Fri, 30 Sep 2016 13:35:14 +0000";s:15:"x-frame-options";s:10:"SAMEORIGIN";s:4:"x-nc";s:11:"HIT lax 250";s:16:"content-encoding";s:4:"gzip";}}s:5:"build";s:14:"20130911000210";}', 'no'),
(183, '_transient_timeout_feed_mod_b9388c83948825c1edaef0d856b7b109', '1475286637', 'no'),
(184, '_transient_feed_mod_b9388c83948825c1edaef0d856b7b109', '1475243437', 'no'),
(185, '_transient_timeout_dash_f69de0bbfe7eaa113146875f40c02000', '1475286637', 'no'),
(186, '_transient_dash_f69de0bbfe7eaa113146875f40c02000', '<div class="rss-widget"><ul><li><a class=''rsswidget'' href=''https://wordpress.org/news/2016/09/wordpress-4-6-1-security-and-maintenance-release/''>WordPress 4.6.1 Security and Maintenance Release</a> <span class="rss-date">07.09.2016</span><div class="rssSummary">WordPress 4.6.1 is now available. This is a security release for all previous versions and we strongly encourage you to update your sites immediately. WordPress versions 4.6 and earlier are affected by two security issues: a cross-site scripting vulnerability via image filename, reported by SumOfPwn researcher Cengiz Han Sahin; and a path traversal vulnerability in [&hellip;]</div></li></ul></div><div class="rss-widget"><ul><li><a class=''rsswidget'' href=''http://heropress.com/essays/rebirth/''>HeroPress: Rebirth</a></li><li><a class=''rsswidget'' href=''https://poststatus.com/art-self-employed-web-consultant-draft-podcast/''>Post Status: The art of being a self-employed web consultant — Draft podcast</a></li><li><a class=''rsswidget'' href=''http://heropress.com/essays/growing-up-with-wordpress/''>HeroPress: Growing Up With WordPress</a></li></ul></div><div class="rss-widget"><ul><li class="dashboard-news-plugin"><span>Популярный плагин:</span> Page Builder by SiteOrigin&nbsp;<a href="plugin-install.php?tab=plugin-information&amp;plugin=siteorigin-panels&amp;_wpnonce=145212024d&amp;TB_iframe=true&amp;width=600&amp;height=800" class="thickbox open-plugin-details-modal" aria-label="Установить Page Builder by SiteOrigin">(Установить)</a></li></ul></div>', 'no'),
(191, '_site_transient_timeout_theme_roots', '1475253345', 'no'),
(192, '_site_transient_theme_roots', 'a:1:{s:12:"ankorservice";s:7:"/themes";}', 'no'),
(194, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1475252490;s:7:"checked";a:5:{s:30:"advanced-custom-fields/acf.php";s:5:"4.4.9";s:27:"acf-gallery/acf-gallery.php";s:5:"1.1.1";s:19:"akismet/akismet.php";s:3:"3.2";s:9:"hello.php";s:3:"1.6";s:36:"wp-pagination/jpages- pagination.php";s:3:"1.6";}s:8:"response";a:0:{}s:12:"translations";a:0:{}s:9:"no_update";a:4:{s:30:"advanced-custom-fields/acf.php";O:8:"stdClass":6:{s:2:"id";s:5:"21367";s:4:"slug";s:22:"advanced-custom-fields";s:6:"plugin";s:30:"advanced-custom-fields/acf.php";s:11:"new_version";s:5:"4.4.9";s:3:"url";s:53:"https://wordpress.org/plugins/advanced-custom-fields/";s:7:"package";s:71:"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.9.zip";}s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:3:"3.2";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:54:"https://downloads.wordpress.org/plugin/akismet.3.2.zip";}s:9:"hello.php";O:8:"stdClass":6:{s:2:"id";s:4:"3564";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip";}s:36:"wp-pagination/jpages- pagination.php";O:8:"stdClass":7:{s:2:"id";s:5:"47074";s:4:"slug";s:13:"wp-pagination";s:6:"plugin";s:36:"wp-pagination/jpages- pagination.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:44:"https://wordpress.org/plugins/wp-pagination/";s:7:"package";s:60:"https://downloads.wordpress.org/plugin/wp-pagination.1.6.zip";s:14:"upgrade_notice";s:54:"Fixed the range.end bug for images per page in Gallery";}}}', 'no');

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_postmeta`
--

CREATE TABLE IF NOT EXISTS `uyua_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB AUTO_INCREMENT=359 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `uyua_postmeta`
--

INSERT INTO `uyua_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 5, '_edit_last', '1'),
(3, 5, '_edit_lock', '1475081031:1'),
(6, 10, '_edit_last', '1'),
(7, 10, 'field_57ed310e9d7d7', 'a:10:{s:3:"key";s:19:"field_57ed310e9d7d7";s:5:"label";s:53:"Логотип или иное изображение";s:4:"name";s:53:"логотип_или_иное_изображение";s:4:"type";s:7:"gallery";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(9, 10, 'position', 'normal'),
(10, 10, 'layout', 'no_box'),
(11, 10, 'hide_on_screen', ''),
(12, 10, '_edit_lock', '1475169576:1'),
(13, 10, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:4:"post";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(14, 10, 'rule', 'a:5:{s:5:"param";s:13:"post_category";s:8:"operator";s:2:"==";s:5:"value";s:1:"3";s:8:"order_no";i:1;s:8:"group_no";i:0;}'),
(20, 13, '_edit_last', '1'),
(21, 13, '_edit_lock', '1475162522:1'),
(22, 15, '_edit_last', '1'),
(23, 15, '_edit_lock', '1475162590:1'),
(24, 17, '_edit_last', '1'),
(25, 17, '_edit_lock', '1475162626:1'),
(26, 19, '_edit_last', '1'),
(27, 19, '_edit_lock', '1475162650:1'),
(28, 21, '_edit_last', '1'),
(29, 21, '_edit_lock', '1475162730:1'),
(30, 23, '_edit_last', '1'),
(31, 23, '_edit_lock', '1475251586:1'),
(32, 26, '_edit_last', '1'),
(33, 26, 'field_57ed3339d8dda', 'a:10:{s:3:"key";s:19:"field_57ed3339d8dda";s:5:"label";s:8:"Фото";s:4:"name";s:5:"photo";s:4:"type";s:7:"gallery";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(36, 26, 'position', 'normal'),
(37, 26, 'layout', 'no_box'),
(38, 26, 'hide_on_screen', ''),
(39, 26, '_edit_lock', '1475169302:1'),
(40, 27, '_edit_last', '1'),
(41, 27, '_edit_lock', '1475171092:1'),
(45, 29, '_edit_last', '1'),
(46, 29, '_edit_lock', '1475164259:1'),
(50, 26, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:4:"page";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(51, 26, 'rule', 'a:5:{s:5:"param";s:4:"page";s:8:"operator";s:2:"==";s:5:"value";s:2:"23";s:8:"order_no";i:1;s:8:"group_no";i:0;}'),
(52, 26, 'rule', 'a:5:{s:5:"param";s:4:"page";s:8:"operator";s:2:"==";s:5:"value";s:2:"27";s:8:"order_no";i:2;s:8:"group_no";i:0;}'),
(53, 26, 'rule', 'a:5:{s:5:"param";s:4:"page";s:8:"operator";s:2:"==";s:5:"value";s:2:"29";s:8:"order_no";i:3;s:8:"group_no";i:0;}'),
(54, 31, '_menu_item_type', 'post_type'),
(55, 31, '_menu_item_menu_item_parent', '0'),
(56, 31, '_menu_item_object_id', '5'),
(57, 31, '_menu_item_object', 'page'),
(58, 31, '_menu_item_target', ''),
(59, 31, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(60, 31, '_menu_item_xfn', ''),
(61, 31, '_menu_item_url', ''),
(63, 32, '_menu_item_type', 'post_type'),
(64, 32, '_menu_item_menu_item_parent', '0'),
(65, 32, '_menu_item_object_id', '23'),
(66, 32, '_menu_item_object', 'page'),
(67, 32, '_menu_item_target', ''),
(68, 32, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(69, 32, '_menu_item_xfn', ''),
(70, 32, '_menu_item_url', ''),
(72, 33, '_menu_item_type', 'post_type'),
(73, 33, '_menu_item_menu_item_parent', '0'),
(74, 33, '_menu_item_object_id', '27'),
(75, 33, '_menu_item_object', 'page'),
(76, 33, '_menu_item_target', ''),
(77, 33, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(78, 33, '_menu_item_xfn', ''),
(79, 33, '_menu_item_url', ''),
(81, 34, '_menu_item_type', 'post_type'),
(82, 34, '_menu_item_menu_item_parent', '0'),
(83, 34, '_menu_item_object_id', '21'),
(84, 34, '_menu_item_object', 'page'),
(85, 34, '_menu_item_target', ''),
(86, 34, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(87, 34, '_menu_item_xfn', ''),
(88, 34, '_menu_item_url', ''),
(90, 36, '_menu_item_type', 'post_type'),
(91, 36, '_menu_item_menu_item_parent', '0'),
(92, 36, '_menu_item_object_id', '29'),
(93, 36, '_menu_item_object', 'page'),
(94, 36, '_menu_item_target', ''),
(95, 36, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(96, 36, '_menu_item_xfn', ''),
(97, 36, '_menu_item_url', ''),
(99, 37, '_menu_item_type', 'post_type'),
(100, 37, '_menu_item_menu_item_parent', '0'),
(101, 37, '_menu_item_object_id', '19'),
(102, 37, '_menu_item_object', 'page'),
(103, 37, '_menu_item_target', ''),
(104, 37, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(105, 37, '_menu_item_xfn', ''),
(106, 37, '_menu_item_url', ''),
(108, 38, '_menu_item_type', 'post_type'),
(109, 38, '_menu_item_menu_item_parent', '0'),
(110, 38, '_menu_item_object_id', '17'),
(111, 38, '_menu_item_object', 'page'),
(112, 38, '_menu_item_target', ''),
(113, 38, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(114, 38, '_menu_item_xfn', ''),
(115, 38, '_menu_item_url', ''),
(117, 39, '_menu_item_type', 'post_type'),
(118, 39, '_menu_item_menu_item_parent', '0'),
(119, 39, '_menu_item_object_id', '15'),
(120, 39, '_menu_item_object', 'page'),
(121, 39, '_menu_item_target', ''),
(122, 39, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(123, 39, '_menu_item_xfn', ''),
(124, 39, '_menu_item_url', ''),
(126, 40, '_menu_item_type', 'post_type'),
(127, 40, '_menu_item_menu_item_parent', '0'),
(128, 40, '_menu_item_object_id', '13'),
(129, 40, '_menu_item_object', 'page'),
(130, 40, '_menu_item_target', ''),
(131, 40, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(132, 40, '_menu_item_xfn', ''),
(133, 40, '_menu_item_url', ''),
(139, 42, '_edit_last', '1'),
(140, 42, '_edit_lock', '1475166364:1'),
(145, 42, '_wp_old_slug', '%d0%b7%d0%b0%d0%b3%d0%be%d0%bb%d0%be%d0%b2%d0%be%d0%ba-%d0%bd%d0%be%d0%b2%d0%be%d1%81%d1%82%d0%b8-1'),
(146, 44, '_edit_last', '1'),
(147, 44, '_edit_lock', '1475166377:1'),
(150, 46, '_edit_last', '1'),
(151, 46, '_edit_lock', '1475166389:1'),
(154, 48, '_edit_last', '1'),
(155, 48, '_edit_lock', '1475168489:1'),
(168, 54, '_wp_attached_file', '2016/09/news.jpg'),
(169, 54, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:240;s:6:"height";i:115;s:4:"file";s:16:"2016/09/news.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"news-150x115.jpg";s:5:"width";i:150;s:6:"height";i:115;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(170, 42, '_thumbnail_id', '54'),
(173, 44, '_thumbnail_id', '54'),
(176, 46, '_thumbnail_id', '54'),
(179, 48, '_thumbnail_id', '54'),
(182, 56, '_wp_attached_file', '2016/09/part_1.jpg'),
(183, 56, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:240;s:6:"height";i:148;s:4:"file";s:18:"2016/09/part_1.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"part_1-150x148.jpg";s:5:"width";i:150;s:6:"height";i:148;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(184, 57, '_wp_attached_file', '2016/09/part_2.jpg'),
(185, 57, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:240;s:6:"height";i:148;s:4:"file";s:18:"2016/09/part_2.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"part_2-150x148.jpg";s:5:"width";i:150;s:6:"height";i:148;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(186, 58, '_wp_attached_file', '2016/09/part_3.jpg'),
(187, 58, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:240;s:6:"height";i:148;s:4:"file";s:18:"2016/09/part_3.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"part_3-150x148.jpg";s:5:"width";i:150;s:6:"height";i:148;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(188, 59, '_wp_attached_file', '2016/09/part_4.jpg'),
(189, 59, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:240;s:6:"height";i:148;s:4:"file";s:18:"2016/09/part_4.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"part_4-150x148.jpg";s:5:"width";i:150;s:6:"height";i:148;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(190, 60, '_wp_attached_file', '2016/09/part_5.jpg'),
(191, 60, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:240;s:6:"height";i:148;s:4:"file";s:18:"2016/09/part_5.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"part_5-150x148.jpg";s:5:"width";i:150;s:6:"height";i:148;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(192, 61, '_wp_attached_file', '2016/09/part_6.jpg'),
(193, 61, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:240;s:6:"height";i:148;s:4:"file";s:18:"2016/09/part_6.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"part_6-150x148.jpg";s:5:"width";i:150;s:6:"height";i:148;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(194, 55, '_edit_last', '1'),
(195, 55, '_edit_lock', '1475168634:1'),
(196, 55, '_thumbnail_id', '61'),
(199, 62, 'логотип_или_иное_изображение', ''),
(200, 62, '_логотип_или_иное_изображение', 'field_57ed310e9d7d7'),
(201, 55, 'логотип_или_иное_изображение', ''),
(202, 55, '_логотип_или_иное_изображение', 'field_57ed310e9d7d7'),
(203, 63, '_edit_last', '1'),
(204, 63, '_thumbnail_id', '60'),
(207, 64, 'логотип_или_иное_изображение', ''),
(208, 64, '_логотип_или_иное_изображение', 'field_57ed310e9d7d7'),
(209, 63, 'логотип_или_иное_изображение', ''),
(210, 63, '_логотип_или_иное_изображение', 'field_57ed310e9d7d7'),
(211, 63, '_edit_lock', '1475168655:1'),
(212, 65, '_edit_last', '1'),
(213, 65, '_edit_lock', '1475168674:1'),
(214, 65, '_thumbnail_id', '59'),
(217, 66, 'логотип_или_иное_изображение', ''),
(218, 66, '_логотип_или_иное_изображение', 'field_57ed310e9d7d7'),
(219, 65, 'логотип_или_иное_изображение', ''),
(220, 65, '_логотип_или_иное_изображение', 'field_57ed310e9d7d7'),
(221, 67, '_edit_last', '1'),
(222, 67, '_edit_lock', '1475168689:1'),
(223, 67, '_thumbnail_id', '58'),
(226, 69, '_edit_last', '1'),
(227, 69, '_thumbnail_id', '57'),
(230, 70, 'логотип_или_иное_изображение', ''),
(231, 70, '_логотип_или_иное_изображение', 'field_57ed310e9d7d7'),
(232, 69, 'логотип_или_иное_изображение', ''),
(233, 69, '_логотип_или_иное_изображение', 'field_57ed310e9d7d7'),
(234, 69, '_edit_lock', '1475168716:1'),
(235, 71, '_edit_last', '1'),
(236, 71, '_thumbnail_id', '56'),
(239, 72, 'логотип_или_иное_изображение', ''),
(240, 72, '_логотип_или_иное_изображение', 'field_57ed310e9d7d7'),
(241, 71, 'логотип_или_иное_изображение', ''),
(242, 71, '_логотип_или_иное_изображение', 'field_57ed310e9d7d7'),
(243, 71, '_edit_lock', '1475168766:1'),
(245, 26, '_wp_trash_meta_status', 'publish'),
(246, 26, '_wp_trash_meta_time', '1475169700'),
(247, 26, '_wp_desired_post_slug', 'acf_%d0%bf%d0%be%d0%bb%d0%b5-%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d0%b8'),
(248, 10, '_wp_trash_meta_status', 'publish'),
(249, 10, '_wp_trash_meta_time', '1475169702'),
(250, 10, '_wp_desired_post_slug', 'acf_%d0%bf%d0%be%d0%bb%d1%8f-%d0%bd%d0%b0%d1%88%d0%b8-%d0%ba%d0%bb%d0%b8%d0%b5%d0%bd%d1%82%d1%8b'),
(251, 73, '_edit_last', '1'),
(252, 73, 'field_57ed4dcad73f3', 'a:10:{s:3:"key";s:19:"field_57ed4dcad73f3";s:5:"label";s:25:"Сканы отзывов";s:4:"name";s:12:"scan_reviews";s:4:"type";s:7:"gallery";s:12:"instructions";s:216:"Добавьте сканы или фотографии отзывов.\r\nБудьте внимательны, в названиях файла не должно быть кириллических символов!!!";s:8:"required";s:1:"1";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:2:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(255, 73, 'position', 'normal'),
(256, 73, 'layout', 'no_box'),
(257, 73, 'hide_on_screen', ''),
(258, 73, '_edit_lock', '1475172889:1'),
(259, 74, '_wp_attached_file', '2016/09/blag.jpg'),
(260, 74, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:661;s:6:"height";i:900;s:4:"file";s:16:"2016/09/blag.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"blag-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"blag-220x300.jpg";s:5:"width";i:220;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(261, 75, '_wp_attached_file', '2016/09/blag-1.jpg'),
(262, 75, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:661;s:6:"height";i:900;s:4:"file";s:18:"2016/09/blag-1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"blag-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"blag-1-220x300.jpg";s:5:"width";i:220;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(263, 76, '_wp_attached_file', '2016/09/blag-2.jpg'),
(264, 76, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:661;s:6:"height";i:900;s:4:"file";s:18:"2016/09/blag-2.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"blag-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"blag-2-220x300.jpg";s:5:"width";i:220;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(265, 77, '_wp_attached_file', '2016/09/blag-3.jpg'),
(266, 77, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:661;s:6:"height";i:900;s:4:"file";s:18:"2016/09/blag-3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"blag-3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"blag-3-220x300.jpg";s:5:"width";i:220;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(267, 78, '_wp_attached_file', '2016/09/blag-4.jpg'),
(268, 78, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:661;s:6:"height";i:900;s:4:"file";s:18:"2016/09/blag-4.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"blag-4-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"blag-4-220x300.jpg";s:5:"width";i:220;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(269, 79, '_wp_attached_file', '2016/09/blag-5.jpg'),
(270, 79, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:661;s:6:"height";i:900;s:4:"file";s:18:"2016/09/blag-5.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"blag-5-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"blag-5-220x300.jpg";s:5:"width";i:220;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(271, 80, '_wp_attached_file', '2016/09/blag-6.jpg'),
(272, 80, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:661;s:6:"height";i:900;s:4:"file";s:18:"2016/09/blag-6.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"blag-6-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"blag-6-220x300.jpg";s:5:"width";i:220;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(273, 81, '_wp_attached_file', '2016/09/blag-7.jpg'),
(274, 81, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:661;s:6:"height";i:900;s:4:"file";s:18:"2016/09/blag-7.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"blag-7-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"blag-7-220x300.jpg";s:5:"width";i:220;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(275, 27, 'scan_reviews', 'a:8:{i:0;s:2:"74";i:1;s:2:"75";i:2;s:2:"76";i:3;s:2:"77";i:4;s:2:"78";i:5;s:2:"79";i:6;s:2:"80";i:7;s:2:"81";}'),
(276, 27, '_scan_reviews', 'field_57ed4dcad73f3'),
(287, 82, '_edit_last', '1'),
(288, 82, 'field_57ed5349fc01d', 'a:10:{s:3:"key";s:19:"field_57ed5349fc01d";s:5:"label";s:20:"Фотографии";s:4:"name";s:6:"photos";s:4:"type";s:7:"gallery";s:12:"instructions";s:255:"Загрузите фотографии, которые Вы бы хотели видеть в галерее.\r\nБудьте внимательны, в названиях файла не должно быть кириллических символов!!!";s:8:"required";s:1:"1";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:2:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(291, 82, 'position', 'normal'),
(292, 82, 'layout', 'no_box'),
(293, 82, 'hide_on_screen', ''),
(294, 82, '_edit_lock', '1475173104:1'),
(295, 83, '_wp_attached_file', '2016/09/gal.jpg'),
(296, 83, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:15:"2016/09/gal.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"gal-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"gal-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(297, 84, '_wp_attached_file', '2016/09/gal-1.jpg'),
(298, 84, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:17:"2016/09/gal-1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"gal-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"gal-1-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(299, 85, '_wp_attached_file', '2016/09/gal-2.jpg'),
(300, 85, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:17:"2016/09/gal-2.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"gal-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"gal-2-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(301, 86, '_wp_attached_file', '2016/09/gal-3.jpg'),
(302, 86, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:17:"2016/09/gal-3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"gal-3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"gal-3-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(303, 87, '_wp_attached_file', '2016/09/gal-4.jpg'),
(304, 87, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:17:"2016/09/gal-4.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"gal-4-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"gal-4-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(305, 88, '_wp_attached_file', '2016/09/gal-5.jpg'),
(306, 88, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:17:"2016/09/gal-5.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"gal-5-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"gal-5-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(307, 89, '_wp_attached_file', '2016/09/gal-6.jpg'),
(308, 89, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:17:"2016/09/gal-6.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"gal-6-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"gal-6-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(309, 90, '_wp_attached_file', '2016/09/gal-7.jpg'),
(310, 90, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:17:"2016/09/gal-7.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"gal-7-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"gal-7-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(311, 91, '_wp_attached_file', '2016/09/gal-8.jpg'),
(312, 91, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:17:"2016/09/gal-8.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"gal-8-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"gal-8-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(313, 92, '_wp_attached_file', '2016/09/gal-9.jpg'),
(314, 92, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:17:"2016/09/gal-9.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"gal-9-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"gal-9-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(315, 93, '_wp_attached_file', '2016/09/gal-10.jpg'),
(316, 93, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:18:"2016/09/gal-10.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"gal-10-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"gal-10-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(317, 94, '_wp_attached_file', '2016/09/gal-—-копия-6.jpg'),
(318, 94, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:32:"2016/09/gal-—-копия-6.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(319, 95, '_wp_attached_file', '2016/09/gal-—-копия-7.jpg'),
(320, 95, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:32:"2016/09/gal-—-копия-7.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(321, 96, '_wp_attached_file', '2016/09/gal-—-копия-8.jpg'),
(322, 96, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:32:"2016/09/gal-—-копия-8.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(323, 97, '_wp_attached_file', '2016/09/gal-—-копия-9.jpg'),
(324, 97, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:32:"2016/09/gal-—-копия-9.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(325, 98, '_wp_attached_file', '2016/09/gal-—-копия-10.jpg'),
(326, 98, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:33:"2016/09/gal-—-копия-10.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(327, 99, '_wp_attached_file', '2016/09/gal-—-копия-11.jpg'),
(328, 99, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:33:"2016/09/gal-—-копия-11.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(329, 100, '_wp_attached_file', '2016/09/gal-—-копия-12.jpg'),
(330, 100, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:33:"2016/09/gal-—-копия-12.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(331, 101, '_wp_attached_file', '2016/09/gal-—-копия-13.jpg'),
(332, 101, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:33:"2016/09/gal-—-копия-13.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(333, 102, '_wp_attached_file', '2016/09/gal-—-копия-14.jpg'),
(334, 102, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:33:"2016/09/gal-—-копия-14.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(335, 103, '_wp_attached_file', '2016/09/gal-—-копия.jpg'),
(336, 103, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:30:"2016/09/gal-—-копия.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(337, 104, '_wp_attached_file', '2016/09/gal-11.jpg'),
(338, 104, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:18:"2016/09/gal-11.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"gal-11-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"gal-11-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(339, 105, '_wp_attached_file', '2016/09/gal-—-копия-2.jpg'),
(340, 105, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:32:"2016/09/gal-—-копия-2.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(341, 106, '_wp_attached_file', '2016/09/gal-—-копия-3.jpg'),
(342, 106, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:32:"2016/09/gal-—-копия-3.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(343, 107, '_wp_attached_file', '2016/09/gal-—-копия-4.jpg'),
(344, 107, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:32:"2016/09/gal-—-копия-4.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(345, 108, '_wp_attached_file', '2016/09/gal-—-копия-5.jpg'),
(346, 108, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:300;s:6:"height";i:226;s:4:"file";s:32:"2016/09/gal-—-копия-5.jpg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(349, 73, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:4:"page";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(350, 73, 'rule', 'a:5:{s:5:"param";s:4:"page";s:8:"operator";s:2:"==";s:5:"value";s:2:"27";s:8:"order_no";i:1;s:8:"group_no";i:0;}'),
(351, 23, 'photos', 'a:12:{i:0;s:2:"83";i:1;s:2:"84";i:2;s:2:"85";i:3;s:2:"86";i:4;s:2:"87";i:5;s:2:"88";i:6;s:2:"89";i:7;s:2:"90";i:8;s:2:"91";i:9;s:2:"92";i:10;s:2:"93";i:11;s:3:"104";}'),
(352, 23, '_photos', 'field_57ed5349fc01d'),
(353, 82, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:4:"page";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(354, 82, 'rule', 'a:5:{s:5:"param";s:4:"page";s:8:"operator";s:2:"==";s:5:"value";s:2:"23";s:8:"order_no";i:1;s:8:"group_no";i:0;}');

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_posts`
--

CREATE TABLE IF NOT EXISTS `uyua_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `uyua_posts`
--

INSERT INTO `uyua_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(2, 1, '2016-09-28 19:17:36', '2016-09-28 16:17:36', 'Это пример страницы. От записей в блоге она отличается тем, что остаётся на одном месте и отображается в меню сайта (в большинстве тем). На странице &laquo;Детали&raquo; владельцы сайтов обычно рассказывают о себе потенциальным посетителям. Например, так:\n\n<blockquote>Привет! Днём я курьер, а вечером &#8212; подающий надежды актёр. Это мой блог. Я живу в Ростове-на-Дону, люблю своего пса Джека и пинаколаду. (И ещё попадать под дождь.)</blockquote>\n\n...или так:\n\n<blockquote>Компания &laquo;Штучки XYZ&raquo; была основана в 1971 году и с тех пор производит качественные штучки. Компания находится в Готэм-сити, имеет штат из более чем 2000 сотрудников и приносит много пользы жителям Готэма.</blockquote>\n\nПерейдите <a href="http://slidetest5.webtm.ru/wp-admin/">в консоль</a>, чтобы удалить эту страницу и создать новые. Успехов!', 'Пример страницы', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2016-09-28 19:17:36', '2016-09-28 16:17:36', '', 0, 'http://slidetest5.webtm.ru/?page_id=2', 0, 'page', '', 0),
(3, 1, '2016-09-28 19:18:03', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'open', 'open', '', '', '', '', '2016-09-28 19:18:03', '0000-00-00 00:00:00', '', 0, 'http://slidetest5.webtm.ru/?p=3', 0, 'post', '', 0),
(4, 1, '2016-09-28 19:43:13', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-09-28 19:43:13', '0000-00-00 00:00:00', '', 0, 'http://slidetest5.webtm.ru/?page_id=4', 0, 'page', '', 0),
(5, 1, '2016-09-28 19:44:25', '2016-09-28 16:44:25', 'Компания «АнкорСтрой» занимается комплексным предоставлением\r\nуслуг, связанных с технологией очистки и нанесения современных\r\nантикоррозионных покрытий и гидроизоляционных материалов с применением\r\nновых методов окраски, разработанных специалистами предприятия WIWA (Германия). Высокая квалификация сотрудников компании, прочные партнерские отношения с поставщиками ипроизводителями, широкий ассортимент современных покрытий, оборудования и услуг позволяют учитывать все пожелания заказчика и соответствовать всем требуемым стандартам и\r\nнормам.Работая с заказчиками,мы подбираем именно то окрасочное оборудование и материалы,которые наилучшим образом подходят для решения стоящих перед ними задач, гарантируя качественное выполнение работы в срок.\r\n\r\nОтдельным направлением нашей компании стоит выделить ПРОМЫШЛЕННЫЙ АЛЬПИНИЗМ. Каждый из наших сотрудников-альпинистов имеет необходимые допуски, удостоверения и разрешения на высотные работы различной сложности. Мы строго следим за поддержанием и повышением квалификации альпинистов, регулярно устраиваем внутренние соревнования и спартакиады, что позволяет нашей компании выполнять высотные работы любой сложности от стандартного мытья окон и фасадов до монтажа оптической иллюминации на высоте.\r\n\r\nКомпания «АнкорСтрой» имеет допуски СРО на все декларируемые виды работ, сертификат соответствия ГОСТ Р ИСО 9001-2015 (ISO 9001:2015). Обладает достаточным материально-техническим ресурсом для выполнения работ различной сложности. Важным преимуществом является возможность одновременного проведения работ на объектах строительства в разных регионах РФ.', 'О компании', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2016-09-28 19:46:14', '2016-09-28 16:46:14', '', 0, 'http://slidetest5.webtm.ru/?page_id=5', 0, 'page', '', 0),
(6, 1, '2016-09-28 19:44:25', '2016-09-28 16:44:25', '<h1>О КОМПАНИИ</h1>\r\nКомпания «АнкорСтрой» занимается комплексным предоставлением\r\nуслуг, связанных с технологией очистки и нанесения современных\r\nантикоррозионных покрытий и гидроизоляционных материалов с применением\r\nновых методов окраски, разработанных специалистами предприятия WIWA (Германия). Высокая квалификация сотрудников компании, прочные партнерские отношения с поставщиками ипроизводителями, широкий ассортимент современных покрытий, оборудования и услуг позволяют учитывать все пожелания заказчика и соответствовать всем требуемым стандартам и\r\nнормам.Работая с заказчиками,мы подбираем именно то окрасочное оборудование и материалы,которые наилучшим образом подходят для решения стоящих перед ними задач, гарантируя качественное выполнение работы в срок.\r\n\r\nОтдельным направлением нашей компании стоит выделить ПРОМЫШЛЕННЫЙ АЛЬПИНИЗМ. Каждый из наших сотрудников-альпинистов имеет необходимые допуски, удостоверения и разрешения на высотные работы различной сложности. Мы строго следим за поддержанием и повышением квалификации альпинистов, регулярно устраиваем внутренние соревнования и спартакиады, что позволяет нашей компании выполнять высотные работы любой сложности от стандартного мытья окон и фасадов до монтажа оптической иллюминации на высоте.\r\n\r\nКомпания «АнкорСтрой» имеет допуски СРО на все декларируемые виды работ, сертификат соответствия ГОСТ Р ИСО 9001-2015 (ISO 9001:2015). Обладает достаточным материально-техническим ресурсом для выполнения работ различной сложности. Важным преимуществом является возможность одновременного проведения работ на объектах строительства в разных регионах РФ.', 'О компании', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-09-28 19:44:25', '2016-09-28 16:44:25', '', 5, 'http://slidetest5.webtm.ru/5-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2016-09-28 19:45:36', '2016-09-28 16:45:36', 'Компания «АнкорСтрой» занимается комплексным предоставлением\nуслуг, связанных с технологией очистки и нанесения современных\nантикоррозионных покрытий и гидроизоляционных материалов с применением\nновых методов окраски, разработанных специалистами предприятия WIWA (Германия). Высокая квалификация сотрудников компании, прочные партнерские отношения с поставщиками ипроизводителями, широкий ассортимент современных покрытий, оборудования и услуг позволяют учитывать все пожелания заказчика и соответствовать всем требуемым стандартам и\nнормам.Работая с заказчиками,мы подбираем именно то окрасочное оборудование и материалы,которые наилучшим образом подходят для решения стоящих перед ними задач, гарантируя качественное выполнение работы в срок.\n\nОтдельным направлением нашей компании стоит выделить ПРОМЫШЛЕННЫЙ АЛЬПИНИЗМ. Каждый из наших сотрудников-альпинистов имеет необходимые допуски, удостоверения и разрешения на высотные работы различной сложности. Мы строго следим за поддержанием и повышением квалификации альпинистов, регулярно устраиваем внутренние соревнования и спартакиады, что позволяет нашей компании выполнять высотные работы любой сложности от стандартного мытья окон и фасадов до монтажа оптической иллюминации на высоте.\n\nКомпания «АнкорСтрой» имеет допуски СРО на все декларируемые виды работ, сертификат соответствия ГОСТ Р ИСО 9001-2015 (ISO 9001:2015). Обладает достаточным материально-техническим ресурсом для выполнения работ различной сложности. Важным преимуществом является возможность одновременного проведения работ на объектах строительства в разных регионах РФ.', 'О компании', '', 'inherit', 'closed', 'closed', '', '5-autosave-v1', '', '', '2016-09-28 19:45:36', '2016-09-28 16:45:36', '', 5, 'http://slidetest5.webtm.ru/5-autosave-v1/', 0, 'revision', '', 0),
(8, 1, '2016-09-28 19:46:14', '2016-09-28 16:46:14', 'Компания «АнкорСтрой» занимается комплексным предоставлением\r\nуслуг, связанных с технологией очистки и нанесения современных\r\nантикоррозионных покрытий и гидроизоляционных материалов с применением\r\nновых методов окраски, разработанных специалистами предприятия WIWA (Германия). Высокая квалификация сотрудников компании, прочные партнерские отношения с поставщиками ипроизводителями, широкий ассортимент современных покрытий, оборудования и услуг позволяют учитывать все пожелания заказчика и соответствовать всем требуемым стандартам и\r\nнормам.Работая с заказчиками,мы подбираем именно то окрасочное оборудование и материалы,которые наилучшим образом подходят для решения стоящих перед ними задач, гарантируя качественное выполнение работы в срок.\r\n\r\nОтдельным направлением нашей компании стоит выделить ПРОМЫШЛЕННЫЙ АЛЬПИНИЗМ. Каждый из наших сотрудников-альпинистов имеет необходимые допуски, удостоверения и разрешения на высотные работы различной сложности. Мы строго следим за поддержанием и повышением квалификации альпинистов, регулярно устраиваем внутренние соревнования и спартакиады, что позволяет нашей компании выполнять высотные работы любой сложности от стандартного мытья окон и фасадов до монтажа оптической иллюминации на высоте.\r\n\r\nКомпания «АнкорСтрой» имеет допуски СРО на все декларируемые виды работ, сертификат соответствия ГОСТ Р ИСО 9001-2015 (ISO 9001:2015). Обладает достаточным материально-техническим ресурсом для выполнения работ различной сложности. Важным преимуществом является возможность одновременного проведения работ на объектах строительства в разных регионах РФ.', 'О компании', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2016-09-28 19:46:14', '2016-09-28 16:46:14', '', 5, 'http://slidetest5.webtm.ru/5-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2016-09-29 18:20:33', '2016-09-29 15:20:33', '', 'Поля "Наши клиенты"', '', 'trash', 'closed', 'closed', '', 'acf_%d0%bf%d0%be%d0%bb%d1%8f-%d0%bd%d0%b0%d1%88%d0%b8-%d0%ba%d0%bb%d0%b8%d0%b5%d0%bd%d1%82%d1%8b__trashed', '', '', '2016-09-29 20:21:42', '2016-09-29 17:21:42', '', 0, 'http://slidetest5.webtm.ru/?post_type=acf&#038;p=10', 0, 'acf', '', 0),
(13, 1, '2016-09-29 18:24:23', '2016-09-29 15:24:23', 'Антикоррозионная защита', 'Антикоррозионная защита', '', 'publish', 'closed', 'closed', '', 'corrosion-protection', '', '', '2016-09-29 18:24:23', '2016-09-29 15:24:23', '', 0, 'http://slidetest5.webtm.ru/?page_id=13', 0, 'page', '', 0),
(14, 1, '2016-09-29 18:24:23', '2016-09-29 15:24:23', 'Антикоррозионная защита', 'Антикоррозионная защита', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2016-09-29 18:24:23', '2016-09-29 15:24:23', '', 13, 'http://slidetest5.webtm.ru/13-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2016-09-29 18:25:09', '2016-09-29 15:25:09', 'Огнезащита металлоконструкций', 'Огнезащита металлоконструкций', '', 'publish', 'closed', 'closed', '', 'fire-protection-of-metal-structures', '', '', '2016-09-29 18:25:30', '2016-09-29 15:25:30', '', 0, 'http://slidetest5.webtm.ru/?page_id=15', 0, 'page', '', 0),
(16, 1, '2016-09-29 18:25:09', '2016-09-29 15:25:09', 'Огнезащита металлоконструкций', 'Огнезащита металлоконструкций', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2016-09-29 18:25:09', '2016-09-29 15:25:09', '', 15, 'http://slidetest5.webtm.ru/15-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2016-09-29 18:25:51', '2016-09-29 15:25:51', 'Подготовка поверхности', 'Подготовка поверхности', '', 'publish', 'closed', 'closed', '', 'surface-preparation', '', '', '2016-09-29 18:26:00', '2016-09-29 15:26:00', '', 0, 'http://slidetest5.webtm.ru/?page_id=17', 0, 'page', '', 0),
(18, 1, '2016-09-29 18:25:51', '2016-09-29 15:25:51', 'Подготовка поверхности', 'Подготовка поверхности', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2016-09-29 18:25:51', '2016-09-29 15:25:51', '', 17, 'http://slidetest5.webtm.ru/17-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2016-09-29 18:26:24', '2016-09-29 15:26:24', 'Промышленный Альпинизм', 'Промышленный Альпинизм', '', 'publish', 'closed', 'closed', '', 'industrial-alpinism', '', '', '2016-09-29 18:26:24', '2016-09-29 15:26:24', '', 0, 'http://slidetest5.webtm.ru/?page_id=19', 0, 'page', '', 0),
(20, 1, '2016-09-29 18:26:24', '2016-09-29 15:26:24', 'Промышленный Альпинизм', 'Промышленный Альпинизм', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2016-09-29 18:26:24', '2016-09-29 15:26:24', '', 19, 'http://slidetest5.webtm.ru/19-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2016-09-29 18:26:50', '2016-09-29 15:26:50', 'Контакты', 'Контакты', '', 'publish', 'closed', 'closed', '', 'contacts', '', '', '2016-09-29 18:26:50', '2016-09-29 15:26:50', '', 0, 'http://slidetest5.webtm.ru/?page_id=21', 0, 'page', '', 0),
(22, 1, '2016-09-29 18:26:50', '2016-09-29 15:26:50', 'Контакты', 'Контакты', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2016-09-29 18:26:50', '2016-09-29 15:26:50', '', 21, 'http://slidetest5.webtm.ru/21-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2016-09-29 18:28:15', '2016-09-29 15:28:15', '', 'Галерея', '', 'publish', 'closed', 'closed', '', 'gallery', '', '', '2016-09-29 20:50:04', '2016-09-29 17:50:04', '', 0, 'http://slidetest5.webtm.ru/?page_id=23', 0, 'page', '', 0),
(24, 1, '2016-09-29 18:28:15', '2016-09-29 15:28:15', '', 'Галерея', '', 'inherit', 'closed', 'closed', '', '23-revision-v1', '', '', '2016-09-29 18:28:15', '2016-09-29 15:28:15', '', 23, 'http://slidetest5.webtm.ru/23-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2016-09-29 18:28:22', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2016-09-29 18:28:22', '0000-00-00 00:00:00', '', 0, 'http://slidetest5.webtm.ru/?post_type=acf&p=25', 0, 'acf', '', 0),
(26, 1, '2016-09-29 18:29:30', '2016-09-29 15:29:30', '', 'Поле галереи', '', 'trash', 'closed', 'closed', '', 'acf_%d0%bf%d0%be%d0%bb%d0%b5-%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d0%b8__trashed', '', '', '2016-09-29 20:21:40', '2016-09-29 17:21:40', '', 0, 'http://slidetest5.webtm.ru/?post_type=acf&#038;p=26', 0, 'acf', '', 0),
(27, 1, '2016-09-29 18:31:23', '2016-09-29 15:31:23', '', 'Отзывы', '', 'publish', 'closed', 'closed', '', 'reviews', '', '', '2016-09-29 20:24:26', '2016-09-29 17:24:26', '', 0, 'http://slidetest5.webtm.ru/?page_id=27', 0, 'page', '', 0),
(28, 1, '2016-09-29 18:31:23', '2016-09-29 15:31:23', '', 'Отзывы', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2016-09-29 18:31:23', '2016-09-29 15:31:23', '', 27, 'http://slidetest5.webtm.ru/27-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2016-09-29 18:32:37', '2016-09-29 15:32:37', '', 'Лицензии и сертификаты', '', 'publish', 'closed', 'closed', '', 'licenses', '', '', '2016-09-29 18:50:59', '2016-09-29 15:50:59', '', 0, 'http://slidetest5.webtm.ru/?page_id=29', 0, 'page', '', 0),
(30, 1, '2016-09-29 18:32:37', '2016-09-29 15:32:37', '', 'Лицензии', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2016-09-29 18:32:37', '2016-09-29 15:32:37', '', 29, 'http://slidetest5.webtm.ru/29-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2016-09-29 18:50:36', '2016-09-29 15:50:36', ' ', '', '', 'publish', 'closed', 'closed', '', '31', '', '', '2016-09-29 18:52:24', '2016-09-29 15:52:24', '', 0, 'http://slidetest5.webtm.ru/?p=31', 1, 'nav_menu_item', '', 0),
(32, 1, '2016-09-29 18:50:36', '2016-09-29 15:50:36', ' ', '', '', 'publish', 'closed', 'closed', '', '32', '', '', '2016-09-29 18:52:24', '2016-09-29 15:52:24', '', 0, 'http://slidetest5.webtm.ru/?p=32', 2, 'nav_menu_item', '', 0),
(33, 1, '2016-09-29 18:50:36', '2016-09-29 15:50:36', ' ', '', '', 'publish', 'closed', 'closed', '', '33', '', '', '2016-09-29 18:52:24', '2016-09-29 15:52:24', '', 0, 'http://slidetest5.webtm.ru/?p=33', 3, 'nav_menu_item', '', 0),
(34, 1, '2016-09-29 18:50:36', '2016-09-29 15:50:36', ' ', '', '', 'publish', 'closed', 'closed', '', '34', '', '', '2016-09-29 18:52:24', '2016-09-29 15:52:24', '', 0, 'http://slidetest5.webtm.ru/?p=34', 4, 'nav_menu_item', '', 0),
(35, 1, '2016-09-29 18:50:59', '2016-09-29 15:50:59', '', 'Лицензии и сертификаты', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2016-09-29 18:50:59', '2016-09-29 15:50:59', '', 29, 'http://slidetest5.webtm.ru/29-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2016-09-29 18:52:24', '2016-09-29 15:52:24', ' ', '', '', 'publish', 'closed', 'closed', '', '36', '', '', '2016-09-29 18:52:24', '2016-09-29 15:52:24', '', 0, 'http://slidetest5.webtm.ru/?p=36', 5, 'nav_menu_item', '', 0),
(37, 1, '2016-09-29 18:53:27', '2016-09-29 15:53:27', ' ', '', '', 'publish', 'closed', 'closed', '', '37', '', '', '2016-09-29 18:53:27', '2016-09-29 15:53:27', '', 0, 'http://slidetest5.webtm.ru/?p=37', 4, 'nav_menu_item', '', 0),
(38, 1, '2016-09-29 18:53:27', '2016-09-29 15:53:27', ' ', '', '', 'publish', 'closed', 'closed', '', '38', '', '', '2016-09-29 18:53:27', '2016-09-29 15:53:27', '', 0, 'http://slidetest5.webtm.ru/?p=38', 3, 'nav_menu_item', '', 0),
(39, 1, '2016-09-29 18:53:27', '2016-09-29 15:53:27', ' ', '', '', 'publish', 'closed', 'closed', '', '39', '', '', '2016-09-29 18:53:27', '2016-09-29 15:53:27', '', 0, 'http://slidetest5.webtm.ru/?p=39', 2, 'nav_menu_item', '', 0),
(40, 1, '2016-09-29 18:53:27', '2016-09-29 15:53:27', ' ', '', '', 'publish', 'closed', 'closed', '', '40', '', '', '2016-09-29 18:53:27', '2016-09-29 15:53:27', '', 0, 'http://slidetest5.webtm.ru/?p=40', 1, 'nav_menu_item', '', 0),
(42, 1, '2016-09-29 19:19:28', '2016-09-29 16:19:28', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #1', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'publish', 'open', 'open', '', 'news-1', '', '', '2016-09-29 19:28:25', '2016-09-29 16:28:25', '', 0, 'http://slidetest5.webtm.ru/?p=42', 0, 'post', '', 0),
(43, 1, '2016-09-29 19:19:28', '2016-09-29 16:19:28', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #1', '', 'inherit', 'closed', 'closed', '', '42-revision-v1', '', '', '2016-09-29 19:19:28', '2016-09-29 16:19:28', '', 42, 'http://slidetest5.webtm.ru/42-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2016-09-29 19:23:14', '2016-09-29 16:23:14', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #2', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'publish', 'open', 'open', '', 'news-2', '', '', '2016-09-29 19:28:38', '2016-09-29 16:28:38', '', 0, 'http://slidetest5.webtm.ru/?p=44', 0, 'post', '', 0),
(45, 1, '2016-09-29 19:23:14', '2016-09-29 16:23:14', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #2', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2016-09-29 19:23:14', '2016-09-29 16:23:14', '', 44, 'http://slidetest5.webtm.ru/44-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2016-09-29 19:23:34', '2016-09-29 16:23:34', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #3', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'publish', 'open', 'open', '', '%d0%b7%d0%b0%d0%b3%d0%be%d0%bb%d0%be%d0%b2%d0%be%d0%ba-%d0%bd%d0%be%d0%b2%d0%be%d1%81%d1%82%d0%b8-3', '', '', '2016-09-29 19:28:51', '2016-09-29 16:28:51', '', 0, 'http://slidetest5.webtm.ru/?p=46', 0, 'post', '', 0),
(47, 1, '2016-09-29 19:23:34', '2016-09-29 16:23:34', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #3', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2016-09-29 19:23:34', '2016-09-29 16:23:34', '', 46, 'http://slidetest5.webtm.ru/46-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2016-09-29 19:24:08', '2016-09-29 16:24:08', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #4', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'publish', 'open', 'open', '', '%d0%b7%d0%b0%d0%b3%d0%be%d0%bb%d0%be%d0%b2%d0%be%d0%ba-%d0%bd%d0%be%d0%b2%d0%be%d1%81%d1%82%d0%b8-4', '', '', '2016-09-29 19:29:03', '2016-09-29 16:29:03', '', 0, 'http://slidetest5.webtm.ru/?p=48', 0, 'post', '', 0),
(49, 1, '2016-09-29 19:24:08', '2016-09-29 16:24:08', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #4', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2016-09-29 19:24:08', '2016-09-29 16:24:08', '', 48, 'http://slidetest5.webtm.ru/48-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2016-09-29 19:25:37', '2016-09-29 16:25:37', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #1', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'inherit', 'closed', 'closed', '', '42-revision-v1', '', '', '2016-09-29 19:25:37', '2016-09-29 16:25:37', '', 42, 'http://slidetest5.webtm.ru/42-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2016-09-29 19:26:00', '2016-09-29 16:26:00', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #4', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2016-09-29 19:26:00', '2016-09-29 16:26:00', '', 48, 'http://slidetest5.webtm.ru/48-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2016-09-29 19:26:03', '2016-09-29 16:26:03', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #3', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2016-09-29 19:26:03', '2016-09-29 16:26:03', '', 46, 'http://slidetest5.webtm.ru/46-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2016-09-29 19:26:07', '2016-09-29 16:26:07', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'Заголовок новости #2', 'С начала сотрудничества с компанией Вендопро Нижнетагильская мебельная фабрика убедилась в высоком профессионализме работников решении задач своих клиентов. С ее помощью продвижение продуктов нашей мебельной фабрики в канале онлайн – продаж увеличилось в 2 раза. Размещение контекстной рекламы оправдало возлагаемые на Яндекс.Директ ожидания и показало высокую эффективность и отдачу.', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2016-09-29 19:26:07', '2016-09-29 16:26:07', '', 44, 'http://slidetest5.webtm.ru/44-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2016-09-29 19:28:13', '2016-09-29 16:28:13', '', 'news', '', 'inherit', 'open', 'closed', '', 'news', '', '', '2016-09-29 19:28:13', '2016-09-29 16:28:13', '', 42, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/news.jpg', 0, 'attachment', 'image/jpeg', 0),
(55, 1, '2016-09-29 20:06:15', '2016-09-29 17:06:15', '', 'НПО Сатурн', '', 'publish', 'open', 'open', '', '%d0%bd%d0%bf%d0%be-%d1%81%d0%b0%d1%82%d1%83%d1%80%d0%bd', '', '', '2016-09-29 20:06:15', '2016-09-29 17:06:15', '', 0, 'http://slidetest5.webtm.ru/?p=55', 0, 'post', '', 0),
(56, 1, '2016-09-29 20:05:34', '2016-09-29 17:05:34', '', 'part_1', '', 'inherit', 'open', 'closed', '', 'part_1', '', '', '2016-09-29 20:05:34', '2016-09-29 17:05:34', '', 55, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/part_1.jpg', 0, 'attachment', 'image/jpeg', 0),
(57, 1, '2016-09-29 20:05:34', '2016-09-29 17:05:34', '', 'part_2', '', 'inherit', 'open', 'closed', '', 'part_2', '', '', '2016-09-29 20:05:34', '2016-09-29 17:05:34', '', 55, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/part_2.jpg', 0, 'attachment', 'image/jpeg', 0),
(58, 1, '2016-09-29 20:05:35', '2016-09-29 17:05:35', '', 'part_3', '', 'inherit', 'open', 'closed', '', 'part_3', '', '', '2016-09-29 20:05:35', '2016-09-29 17:05:35', '', 55, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/part_3.jpg', 0, 'attachment', 'image/jpeg', 0),
(59, 1, '2016-09-29 20:05:35', '2016-09-29 17:05:35', '', 'part_4', '', 'inherit', 'open', 'closed', '', 'part_4', '', '', '2016-09-29 20:05:35', '2016-09-29 17:05:35', '', 55, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/part_4.jpg', 0, 'attachment', 'image/jpeg', 0),
(60, 1, '2016-09-29 20:05:36', '2016-09-29 17:05:36', '', 'part_5', '', 'inherit', 'open', 'closed', '', 'part_5', '', '', '2016-09-29 20:05:36', '2016-09-29 17:05:36', '', 55, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/part_5.jpg', 0, 'attachment', 'image/jpeg', 0),
(61, 1, '2016-09-29 20:05:36', '2016-09-29 17:05:36', '', 'part_6', '', 'inherit', 'open', 'closed', '', 'part_6', '', '', '2016-09-29 20:05:36', '2016-09-29 17:05:36', '', 55, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/part_6.jpg', 0, 'attachment', 'image/jpeg', 0),
(62, 1, '2016-09-29 20:06:15', '2016-09-29 17:06:15', '', 'НПО Сатурн', '', 'inherit', 'closed', 'closed', '', '55-revision-v1', '', '', '2016-09-29 20:06:15', '2016-09-29 17:06:15', '', 55, 'http://slidetest5.webtm.ru/55-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2016-09-29 20:06:36', '2016-09-29 17:06:36', '', 'Группа компаний Альтаир', '', 'publish', 'open', 'open', '', '%d0%b3%d1%80%d1%83%d0%bf%d0%bf%d0%b0-%d0%ba%d0%be%d0%bc%d0%bf%d0%b0%d0%bd%d0%b8%d0%b9-%d0%b0%d0%bb%d1%8c%d1%82%d0%b0%d0%b8%d1%80', '', '', '2016-09-29 20:06:36', '2016-09-29 17:06:36', '', 0, 'http://slidetest5.webtm.ru/?p=63', 0, 'post', '', 0),
(64, 1, '2016-09-29 20:06:36', '2016-09-29 17:06:36', '', 'Группа компаний Альтаир', '', 'inherit', 'closed', 'closed', '', '63-revision-v1', '', '', '2016-09-29 20:06:36', '2016-09-29 17:06:36', '', 63, 'http://slidetest5.webtm.ru/63-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2016-09-29 20:06:53', '2016-09-29 17:06:53', '', 'Леруа', '', 'publish', 'open', 'open', '', '%d0%bb%d0%b5%d1%80%d1%83%d0%b0', '', '', '2016-09-29 20:06:53', '2016-09-29 17:06:53', '', 0, 'http://slidetest5.webtm.ru/?p=65', 0, 'post', '', 0),
(66, 1, '2016-09-29 20:06:53', '2016-09-29 17:06:53', '', 'Леруа', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2016-09-29 20:06:53', '2016-09-29 17:06:53', '', 65, 'http://slidetest5.webtm.ru/65-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2016-09-29 20:07:10', '2016-09-29 17:07:10', '', 'РЖД', '', 'publish', 'open', 'open', '', '%d1%80%d0%b6%d0%b4', '', '', '2016-09-29 20:07:10', '2016-09-29 17:07:10', '', 0, 'http://slidetest5.webtm.ru/?p=67', 0, 'post', '', 0),
(68, 1, '2016-09-29 20:07:10', '2016-09-29 17:07:10', '', 'РЖД', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2016-09-29 20:07:10', '2016-09-29 17:07:10', '', 67, 'http://slidetest5.webtm.ru/67-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2016-09-29 20:07:35', '2016-09-29 17:07:35', '', 'Роснефть', '', 'publish', 'open', 'open', '', '%d1%80%d0%be%d1%81%d0%bd%d0%b5%d1%84%d1%82%d1%8c', '', '', '2016-09-29 20:07:35', '2016-09-29 17:07:35', '', 0, 'http://slidetest5.webtm.ru/?p=69', 0, 'post', '', 0),
(70, 1, '2016-09-29 20:07:35', '2016-09-29 17:07:35', '', 'Роснефть', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2016-09-29 20:07:35', '2016-09-29 17:07:35', '', 69, 'http://slidetest5.webtm.ru/69-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2016-09-29 20:07:54', '2016-09-29 17:07:54', '', 'Газпром', '', 'publish', 'open', 'open', '', '%d0%b3%d0%b0%d0%b7%d0%bf%d1%80%d0%be%d0%bc', '', '', '2016-09-29 20:07:54', '2016-09-29 17:07:54', '', 0, 'http://slidetest5.webtm.ru/?p=71', 0, 'post', '', 0),
(72, 1, '2016-09-29 20:07:54', '2016-09-29 17:07:54', '', 'Газпром', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2016-09-29 20:07:54', '2016-09-29 17:07:54', '', 71, 'http://slidetest5.webtm.ru/71-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2016-09-29 20:23:35', '2016-09-29 17:23:35', '', 'Поля страницы "Отзывы"', '', 'publish', 'closed', 'closed', '', 'acf_%d0%bf%d0%be%d0%bb%d1%8f-%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%86%d1%8b-%d0%be%d1%82%d0%b7%d1%8b%d0%b2%d1%8b', '', '', '2016-09-29 20:49:58', '2016-09-29 17:49:58', '', 0, 'http://slidetest5.webtm.ru/?post_type=acf&#038;p=73', 0, 'acf', '', 0),
(74, 1, '2016-09-29 20:24:02', '2016-09-29 17:24:02', '', 'blag', '', 'inherit', 'open', 'closed', '', 'blag', '', '', '2016-09-29 20:24:02', '2016-09-29 17:24:02', '', 27, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/blag.jpg', 0, 'attachment', 'image/jpeg', 0),
(75, 1, '2016-09-29 20:24:08', '2016-09-29 17:24:08', '', 'blag', '', 'inherit', 'open', 'closed', '', 'blag-2', '', '', '2016-09-29 20:24:08', '2016-09-29 17:24:08', '', 27, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/blag-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(76, 1, '2016-09-29 20:24:12', '2016-09-29 17:24:12', '', 'blag', '', 'inherit', 'open', 'closed', '', 'blag-3', '', '', '2016-09-29 20:24:12', '2016-09-29 17:24:12', '', 27, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/blag-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(77, 1, '2016-09-29 20:24:14', '2016-09-29 17:24:14', '', 'blag', '', 'inherit', 'open', 'closed', '', 'blag-4', '', '', '2016-09-29 20:24:14', '2016-09-29 17:24:14', '', 27, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/blag-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(78, 1, '2016-09-29 20:24:16', '2016-09-29 17:24:16', '', 'blag', '', 'inherit', 'open', 'closed', '', 'blag-5', '', '', '2016-09-29 20:24:16', '2016-09-29 17:24:16', '', 27, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/blag-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(79, 1, '2016-09-29 20:24:17', '2016-09-29 17:24:17', '', 'blag', '', 'inherit', 'open', 'closed', '', 'blag-6', '', '', '2016-09-29 20:24:17', '2016-09-29 17:24:17', '', 27, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/blag-5.jpg', 0, 'attachment', 'image/jpeg', 0),
(80, 1, '2016-09-29 20:24:19', '2016-09-29 17:24:19', '', 'blag', '', 'inherit', 'open', 'closed', '', 'blag-7', '', '', '2016-09-29 20:24:19', '2016-09-29 17:24:19', '', 27, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/blag-6.jpg', 0, 'attachment', 'image/jpeg', 0),
(81, 1, '2016-09-29 20:24:20', '2016-09-29 17:24:20', '', 'blag', '', 'inherit', 'open', 'closed', '', 'blag-8', '', '', '2016-09-29 20:24:20', '2016-09-29 17:24:20', '', 27, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/blag-7.jpg', 0, 'attachment', 'image/jpeg', 0),
(82, 1, '2016-09-29 20:47:06', '2016-09-29 17:47:06', '', 'Поля страницы "Галерея"', '', 'publish', 'closed', 'closed', '', 'acf_%d0%bf%d0%be%d0%bb%d1%8f-%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%86%d1%8b-%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f', '', '', '2016-09-29 21:17:26', '2016-09-29 18:17:26', '', 0, 'http://slidetest5.webtm.ru/?post_type=acf&#038;p=82', 0, 'acf', '', 0),
(83, 1, '2016-09-29 20:47:39', '2016-09-29 17:47:39', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal', '', '', '2016-09-29 20:47:39', '2016-09-29 17:47:39', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal.jpg', 0, 'attachment', 'image/jpeg', 0),
(84, 1, '2016-09-29 20:47:40', '2016-09-29 17:47:40', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-2', '', '', '2016-09-29 20:47:40', '2016-09-29 17:47:40', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(85, 1, '2016-09-29 20:47:41', '2016-09-29 17:47:41', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-3', '', '', '2016-09-29 20:47:41', '2016-09-29 17:47:41', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(86, 1, '2016-09-29 20:47:41', '2016-09-29 17:47:41', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-4', '', '', '2016-09-29 20:47:41', '2016-09-29 17:47:41', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(87, 1, '2016-09-29 20:47:42', '2016-09-29 17:47:42', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-5', '', '', '2016-09-29 20:47:42', '2016-09-29 17:47:42', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(88, 1, '2016-09-29 20:47:43', '2016-09-29 17:47:43', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-6', '', '', '2016-09-29 20:47:43', '2016-09-29 17:47:43', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-5.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2016-09-29 20:47:44', '2016-09-29 17:47:44', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-7', '', '', '2016-09-29 20:47:44', '2016-09-29 17:47:44', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-6.jpg', 0, 'attachment', 'image/jpeg', 0),
(90, 1, '2016-09-29 20:47:44', '2016-09-29 17:47:44', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-8', '', '', '2016-09-29 20:47:44', '2016-09-29 17:47:44', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-7.jpg', 0, 'attachment', 'image/jpeg', 0),
(91, 1, '2016-09-29 20:47:45', '2016-09-29 17:47:45', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-9', '', '', '2016-09-29 20:47:45', '2016-09-29 17:47:45', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-8.jpg', 0, 'attachment', 'image/jpeg', 0),
(92, 1, '2016-09-29 20:47:46', '2016-09-29 17:47:46', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-10', '', '', '2016-09-29 20:47:46', '2016-09-29 17:47:46', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-9.jpg', 0, 'attachment', 'image/jpeg', 0),
(93, 1, '2016-09-29 20:47:47', '2016-09-29 17:47:47', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-11', '', '', '2016-09-29 20:47:47', '2016-09-29 17:47:47', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-10.jpg', 0, 'attachment', 'image/jpeg', 0),
(94, 1, '2016-09-29 20:48:18', '2016-09-29 17:48:18', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-6', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-6', '', '', '2016-09-29 20:48:18', '2016-09-29 17:48:18', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-6.jpg', 0, 'attachment', 'image/jpeg', 0),
(95, 1, '2016-09-29 20:48:19', '2016-09-29 17:48:19', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-7', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-7', '', '', '2016-09-29 20:48:19', '2016-09-29 17:48:19', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-7.jpg', 0, 'attachment', 'image/jpeg', 0),
(96, 1, '2016-09-29 20:48:19', '2016-09-29 17:48:19', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-8', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-8', '', '', '2016-09-29 20:48:19', '2016-09-29 17:48:19', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-8.jpg', 0, 'attachment', 'image/jpeg', 0),
(97, 1, '2016-09-29 20:48:20', '2016-09-29 17:48:20', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-9', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-9', '', '', '2016-09-29 20:48:20', '2016-09-29 17:48:20', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-9.jpg', 0, 'attachment', 'image/jpeg', 0),
(98, 1, '2016-09-29 20:48:21', '2016-09-29 17:48:21', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-10', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-10', '', '', '2016-09-29 20:48:21', '2016-09-29 17:48:21', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-10.jpg', 0, 'attachment', 'image/jpeg', 0),
(99, 1, '2016-09-29 20:48:22', '2016-09-29 17:48:22', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-11', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-11', '', '', '2016-09-29 20:48:22', '2016-09-29 17:48:22', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-11.jpg', 0, 'attachment', 'image/jpeg', 0),
(100, 1, '2016-09-29 20:48:22', '2016-09-29 17:48:22', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-12', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-12', '', '', '2016-09-29 20:48:22', '2016-09-29 17:48:22', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-12.jpg', 0, 'attachment', 'image/jpeg', 0),
(101, 1, '2016-09-29 20:48:23', '2016-09-29 17:48:23', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-13', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-13', '', '', '2016-09-29 20:48:23', '2016-09-29 17:48:23', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-13.jpg', 0, 'attachment', 'image/jpeg', 0),
(102, 1, '2016-09-29 20:48:24', '2016-09-29 17:48:24', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-14', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-14', '', '', '2016-09-29 20:48:24', '2016-09-29 17:48:24', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-14.jpg', 0, 'attachment', 'image/jpeg', 0),
(103, 1, '2016-09-29 20:48:24', '2016-09-29 17:48:24', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f', '', '', '2016-09-29 20:48:24', '2016-09-29 17:48:24', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия.jpg', 0, 'attachment', 'image/jpeg', 0),
(104, 1, '2016-09-29 20:48:25', '2016-09-29 17:48:25', '', 'gal', '', 'inherit', 'open', 'closed', '', 'gal-12', '', '', '2016-09-29 20:48:25', '2016-09-29 17:48:25', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-11.jpg', 0, 'attachment', 'image/jpeg', 0),
(105, 1, '2016-09-29 20:48:25', '2016-09-29 17:48:25', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-2', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-2', '', '', '2016-09-29 20:48:25', '2016-09-29 17:48:25', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(106, 1, '2016-09-29 20:48:26', '2016-09-29 17:48:26', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-3', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-3', '', '', '2016-09-29 20:48:26', '2016-09-29 17:48:26', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(107, 1, '2016-09-29 20:48:27', '2016-09-29 17:48:27', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-4', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-4', '', '', '2016-09-29 20:48:27', '2016-09-29 17:48:27', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(108, 1, '2016-09-29 20:48:27', '2016-09-29 17:48:27', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-5', '', 'inherit', 'open', 'closed', '', 'gal-%d0%ba%d0%be%d0%bf%d0%b8%d1%8f-5', '', '', '2016-09-29 20:48:27', '2016-09-29 17:48:27', '', 23, 'http://slidetest5.webtm.ru/wp-content/uploads/2016/09/gal-—-копия-5.jpg', 0, 'attachment', 'image/jpeg', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_termmeta`
--

CREATE TABLE IF NOT EXISTS `uyua_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_terms`
--

CREATE TABLE IF NOT EXISTS `uyua_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `uyua_terms`
--

INSERT INTO `uyua_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Без рубрики', '%d0%b1%d0%b5%d0%b7-%d1%80%d1%83%d0%b1%d1%80%d0%b8%d0%ba%d0%b8', 0),
(2, 'Новости', 'news', 0),
(3, 'Клиенты', 'clients', 0),
(4, 'Отзывы', 'reviews', 0),
(5, 'Главное меню', '%d0%b3%d0%bb%d0%b0%d0%b2%d0%bd%d0%be%d0%b5-%d0%bc%d0%b5%d0%bd%d1%8e', 0),
(6, 'Второе меню', '%d0%b2%d1%82%d0%be%d1%80%d0%be%d0%b5-%d0%bc%d0%b5%d0%bd%d1%8e', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_term_relationships`
--

CREATE TABLE IF NOT EXISTS `uyua_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `uyua_term_relationships`
--

INSERT INTO `uyua_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(31, 5, 0),
(32, 5, 0),
(33, 5, 0),
(34, 5, 0),
(36, 5, 0),
(37, 6, 0),
(38, 6, 0),
(39, 6, 0),
(40, 6, 0),
(42, 2, 0),
(44, 2, 0),
(46, 2, 0),
(48, 2, 0),
(55, 3, 0),
(63, 3, 0),
(65, 3, 0),
(67, 3, 0),
(69, 3, 0),
(71, 3, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `uyua_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `uyua_term_taxonomy`
--

INSERT INTO `uyua_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'category', 'Категория для новостей на главной странице', 0, 4),
(3, 3, 'category', 'Категория для клиентов на главной странице', 0, 6),
(4, 4, 'category', 'Категория для статей на странице отзывы', 0, 0),
(5, 5, 'nav_menu', '', 0, 5),
(6, 6, 'nav_menu', '', 0, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_usermeta`
--

CREATE TABLE IF NOT EXISTS `uyua_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `uyua_usermeta`
--

INSERT INTO `uyua_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'administrator'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'uyua_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'uyua_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', ''),
(13, 1, 'show_welcome_panel', '1'),
(14, 1, 'session_tokens', 'a:3:{s:64:"27e531b9c7b932634ff5895289c681621d1d99b70a609d8ceaf4f0a469a93bea";a:4:{s:10:"expiration";i:1475252281;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36";s:5:"login";i:1475079481;}s:64:"1c811d585a3b10d75449e95d52800d878a61c270fb2f5c9a9682e32d4cb5e25a";a:4:{s:10:"expiration";i:1475333663;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36";s:5:"login";i:1475160863;}s:64:"9a16b811480280261028fca90828c8637dd288e744eaf58f2c73593d9c04d06a";a:4:{s:10:"expiration";i:1475416227;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36";s:5:"login";i:1475243427;}}'),
(15, 1, 'uyua_dashboard_quick_press_last_post_id', '3'),
(16, 1, 'uyua_user-settings', 'editor=tinymce&libraryContent=browse'),
(17, 1, 'uyua_user-settings-time', '1475166502'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:15:"title-attribute";i:2;s:11:"css-classes";i:3;s:3:"xfn";i:4;s:11:"description";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(20, 1, 'closedpostboxes_post', 'a:0:{}'),
(21, 1, 'metaboxhidden_post', 'a:8:{i:0;s:6:"acf_26";i:1;s:6:"acf_10";i:2;s:13:"trackbacksdiv";i:3;s:10:"postcustom";i:4;s:16:"commentstatusdiv";i:5;s:11:"commentsdiv";i:6;s:7:"slugdiv";i:7;s:9:"authordiv";}'),
(22, 1, 'closedpostboxes_page', 'a:0:{}'),
(23, 1, 'metaboxhidden_page', 'a:6:{i:0;s:6:"acf_73";i:1;s:10:"postcustom";i:2;s:16:"commentstatusdiv";i:3;s:11:"commentsdiv";i:4;s:7:"slugdiv";i:5;s:9:"authordiv";}');

-- --------------------------------------------------------

--
-- Структура таблицы `uyua_users`
--

CREATE TABLE IF NOT EXISTS `uyua_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(255) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `uyua_users`
--

INSERT INTO `uyua_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'administrator', '$P$BLTeS7vs01HXUtQa236xvOFg8wPcwd0', 'administrator', 'symbolistics@gmail.com', '', '2016-09-28 16:17:36', '', 0, 'administrator');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `uyua_commentmeta`
--
ALTER TABLE `uyua_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `uyua_comments`
--
ALTER TABLE `uyua_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Индексы таблицы `uyua_links`
--
ALTER TABLE `uyua_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Индексы таблицы `uyua_options`
--
ALTER TABLE `uyua_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Индексы таблицы `uyua_postmeta`
--
ALTER TABLE `uyua_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `uyua_posts`
--
ALTER TABLE `uyua_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Индексы таблицы `uyua_termmeta`
--
ALTER TABLE `uyua_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `uyua_terms`
--
ALTER TABLE `uyua_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Индексы таблицы `uyua_term_relationships`
--
ALTER TABLE `uyua_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Индексы таблицы `uyua_term_taxonomy`
--
ALTER TABLE `uyua_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Индексы таблицы `uyua_usermeta`
--
ALTER TABLE `uyua_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `uyua_users`
--
ALTER TABLE `uyua_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `uyua_commentmeta`
--
ALTER TABLE `uyua_commentmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `uyua_comments`
--
ALTER TABLE `uyua_comments`
  MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `uyua_links`
--
ALTER TABLE `uyua_links`
  MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `uyua_options`
--
ALTER TABLE `uyua_options`
  MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=199;
--
-- AUTO_INCREMENT для таблицы `uyua_postmeta`
--
ALTER TABLE `uyua_postmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=359;
--
-- AUTO_INCREMENT для таблицы `uyua_posts`
--
ALTER TABLE `uyua_posts`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT для таблицы `uyua_termmeta`
--
ALTER TABLE `uyua_termmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `uyua_terms`
--
ALTER TABLE `uyua_terms`
  MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `uyua_term_taxonomy`
--
ALTER TABLE `uyua_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `uyua_usermeta`
--
ALTER TABLE `uyua_usermeta`
  MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT для таблицы `uyua_users`
--
ALTER TABLE `uyua_users`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
