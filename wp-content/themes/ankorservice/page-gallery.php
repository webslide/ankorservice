<?php get_header("gallery"); ?>
<div class="center">
    <div class="galery">
        <div class="title">
            ГАЛЕРЕЯ
        </div>
        <div id="itemContainer" class="galery_cont popup-gallery">
            <?php
            $images = get_field('photos');
            foreach ($images as $image) {
                echo '<div class="galery_bl">
                                          <a href="' . $image['url'] . '">
                                        <img src="' . $image['sizes']['large'] . '" alt="image">
                                    </a>
                             </div>';
            }
            ?>
        </div>
        <div class="pagination">
            <div class="holder"></div>
        </div>
    </div>
</div>
<?php get_footer(); ?>



