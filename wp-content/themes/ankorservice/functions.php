<?php
add_theme_support('menus');
add_theme_support('post-thumbnails', array('post'));

add_action('wp_ajax_siteWideMessage', 'sendmail');
add_action('wp_ajax_nopriv_siteWideMessage', 'sendmail');

add_filter('nav_menu_css_class','special_nav_class',10,2);

function special_nav_class($classes, $item){
	if(in_array('current-menu-item',$classes)){
		$classes[] = 'act ';
	}
	return $classes;
}

function sendmail()
{
    $admin = get_option('admin_email');

    $name = htmlspecialchars(trim($_REQUEST['name']));
    $phone = htmlspecialchars(trim($_REQUEST['phone']));
    $email = htmlspecialchars(trim($_REQUEST['email']));
    $msg = htmlspecialchars(trim($_REQUEST['msg']));

    $message = "Имя: $name \r\n";
    $message .= "Телефон: $phone \r\n";
    $message .= "Адрес электронной почты: $email \r\n";
    $message .= "Сообщение: $msg";

    $subject = "АнкорСтрой | Новое сообщение";
    $headers = 'From: ' . $admin . '' . "\r\n" .
        'Reply-To: ' . $admin . '' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    $mail = wp_mail($admin, $subject, $message, $headers);
    die();
}