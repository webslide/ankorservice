<?php get_header(); ?>
<div class="center">
    <div class="client">
        <div class="title">
            НАШИ КЛИЕНТЫ
        </div>
        <div class="client_cont">
            <?php
            $args = array('numberposts' => '0', 'category_name' => 'clients', 'order_by' => 'date', 'order' => 'ASC');
            $posts = get_posts($args);
            foreach ($posts as $post) {
                echo '<div class="client_bl">';
                if (has_post_thumbnail()) {
                    $large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
                    echo '<a href="'.get_bloginfo('wpurl').'/reviews" title=""><img src="'.$large_image_url[0].'" alt=""/></a>';
                }
                echo '</div>';
            }
            ?>
        </div>
    </div>
</div>
<div class="news">
    <div class="center">
        <div class="title">
            НОВОСТИ
        </div>

        <div class="news_cont">
            <?php
            $args = array('numberposts' => '4', 'category_name' => 'news', 'order_by' => 'date', 'order' => 'ASC');
            $posts = get_posts($args);
            foreach ($posts as $post) {
                echo '<div class="news_bl">
                        <div class="news_img">';
                if (has_post_thumbnail()) {
                    the_post_thumbnail();
                }
                echo '</div>
                        <div class="news_title">';
                echo '<a href="' . get_permalink() . '" title="">' . the_title() . '</a>';
                echo '</div>
                        <div class="news_text"';
                the_excerpt();
                echo '</div>';
                echo '<div class="news_date">' . get_the_date() . '</div>';
                echo '</div>';
            }
wp_reset_postdata();
            ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
