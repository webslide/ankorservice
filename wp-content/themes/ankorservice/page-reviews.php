<?php get_header('reviews');?>
    <div class="center">
        <div class="galery">
            <div class="title">
                Отзывы
            </div>
            <div class="galery_cont">
                <?php $images = get_field('scan_reviews');
                foreach ($images as $image){
                    echo '<div class="galery_bl">
                                    <a class="image-popup-no-margins" href="'.$image['url'].'">
                    <img src="'.$image['sizes']['large'].'">
                </a>
                </div>';
                }
                ?>
            </div>
        </div>
    </div>
<?php get_footer();?>