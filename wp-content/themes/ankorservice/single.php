<?php get_header();?>
<?php
wp_reset_query();
while (have_posts()): the_post();?>
    <div class="center">
        <div class="text">
            <h1><?php the_title();?></h1>
            <? the_content();?>
            <a href="javascript: history.go(-1)" title="">Назад</a>
        </div>
    </div>
<?php endwhile;
get_footer();?>