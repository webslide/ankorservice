<!DOCTYPE html>
<html <?php language_attributes();?>>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?php echo get_template_directory_uri()?>/favicon.ico" rel="shortcut icon" type="icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/js.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/js_theme.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jPages.min.js"></script>

    <title><?php bloginfo('name');
        echo ' | ';
        the_title(); ?></title>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.popup-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                tLoading: 'Загрузка изображения...',
                mainClass: 'mfp-with-zoom',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1]
                },
                image: {
                    tError: '<a href="%url%">Изображение #%curr%</a> не может быть загружено.'
                }
            });
            $(function() {
                $("div.holder").jPages({
                    containerID : "itemContainer",
                    first       : false,
                    previous    : false,
                    next        : false,
                    perPage     : "9",
                    midRange    : "5"
                });
            });

            $('#form').submit(function (e) {
                var form = $('form#form');
                var data = form.serialize() + '&action=siteWideMessage';
                $.post('<?php echo admin_url('admin-ajax.php');?>', data, function () {
                    alert('Спасибо, Ваше сообщение было отправлено!');
                    form.trigger('reset');
                    setTimeout(function () {
                        $('.close').click();
                    }, 2000);
                });
                return false;
            });
        });
    </script>
</head>
<body>

<div class="overlay"></div>
<div class="popup popup_zvonok">
    <div class="close close_popup"></div>
    <p>Отправить сообщение</p>

    <div class="popup_form">
        <form id="form" method="post" action="">
            <input type="text" name="name" id="name" placeholder="Ваше имя*" onfocus="$(this).attr('placeholder', '')" required onblur="$(this).attr('placeholder', 'Ваше имя*')" />
            <input type="text" name="phone" id="phone" placeholder="Ваш телефон*" onfocus="$(this).attr('placeholder', '')" required onblur="$(this).attr('placeholder', 'Ваш телефон*')" />
            <input type="text" name="email" id="email" placeholder="Ваш почта" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Ваша почта')" />
            <textarea name="msg" placeholder="Ваш сообщение" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Ваше сообщение')"></textarea>
            <input type="submit" value="Отправить" title=""/>
        </form>
    </div>
</div>

<div class="over">
    <div class="header">
        <div class="center">
            <div class="header_shadow">
                <div class="header_box">
                    <div class="main_menu">
                        <?php wp_nav_menu(array('menu' => '5')); ?>
                    </div>
                    <div class="tel">
                        8 (4852) 68-07-61
                    </div>
                    <div class="adres">
                        г.Ярославль<br />
                        Email: ankor.st@mail.ru
                    </div>
                </div>
            </div>
            <div class="logo">
                <a href="/"><img src="<?php echo get_template_directory_uri()?>/images/logo.png" alt=""/></a>
            </div>
            <div class="sub_menu">
                <span>Меню</span>
                <?php wp_nav_menu(array('menu' => '6')); ?>
            </div>
            <div class="call">Обратная связь</div>
        </div>
    </div>