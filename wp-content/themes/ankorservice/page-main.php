<?php
get_header();
wp_reset_query();
query_posts('pagename=main');
while (have_posts()):the_post();
    ?>
    <div class="center">
        <div class="numbers_cont">
            <?php
            function getField(){
                $key = array();
                $value = array();
                $info = array();

                if( get_field("text_in_first_hexagon")){
                    if(get_field("text_under_the_first_hexagon")){
                        array_push($key, get_field("text_in_first_hexagon"));
                        array_push($value, get_field("text_under_the_first_hexagon"));
                    }
                    else return false;
                }
                if( get_field("text_in_second_hexagon")){
                    if(get_field("text_under_the_second_hexagon")){
                        array_push($key, get_field("text_in_second_hexagon"));
                        array_push($value, get_field("text_under_the_second_hexagon"));
                    }
                    else return false;
                }
                if( get_field("text_in_thrid_hexagon")){
                    if(get_field("text_under_the_thrid_hexagon")){
                        array_push($key, get_field("text_in_thrid_hexagon"));
                        array_push($value, get_field("text_under_the_thrid_hexagon"));
                    }
                    else return false;
                }
                if( get_field("text_in_fourth_hexagon")){
                    if(get_field("text_under_the_fourth_hexagon")){
                        array_push($key, get_field("text_in_fourth_hexagon"));
                        array_push($value, get_field("text_under_the_fourth_hexagon"));
                    }
                    else return false;
                }
                for ($i = 0; $i<count($key); $i++) {
                    $info[$key[$i]] = $value[$i];
                }
                return $info;
            }
            /*
            $info = array(
                get_field("text_in_first_hexagon") => get_field("text_under_the_first_hexagon"),
                get_field("text_in_second_hexagon") => get_field("text_under_the_second_hexagon"),
                get_field("text_in_thrid_hexagon") => get_field("text_under_the_thrid_hexagon"),
                get_field("text_in_fourth_hexagon") => get_field("text_under_the_fourth_hexagon")
            );
            */
            foreach (getField() as $keyItem => $valueItem) {
                echo '<div class="numbers"><span>'.$keyItem.'</span><p>'.$valueItem.'</p> </div>';
            }


            ?>
        </div>
    </div>
<?php endwhile;
get_footer(); ?>